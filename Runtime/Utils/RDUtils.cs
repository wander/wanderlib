using System;
using UnityEngine;

namespace Wander
{
    public static class RDUtils
    {
        static readonly double RDOriginX = -285401.92;
        static readonly double RDOriginY = 22598.08;
        static readonly double RDTileSizeTop = 3440.64;
        static readonly double RDDefaultTileRes = 256;

        static double RDPivotX;
        static double RDPivotY;

        static RDUtils()
        {
            // Forum meest rechtse punt: 51.98520265797496, 5.664388463743642
            SetPivot( 51.98520265797496, 5.664388463743642 );
        }

        static void SetPivot( double lat, double lon )
        {
            GPS2RD( lat, lon, out RDPivotX, out RDPivotY );
        }

        // Van graden naar radialen en vice-versa
        const double DEGREES_2_RADIANS = 3.14159265359 / 180.0;

        const double Bessel_e2 = 0.006674372175;
        const double Bessel_a = 6377397.155;
        const double Bessel_b = 6356078.963;

        const double GRS80_e2 = 0.00669437999;
        const double GRS80_a = 6378137.0;
        const double GRS80_b = 6356752.314;

        static Bessel1841 bessel = new Bessel1841();
        static GRS80 grs80 = new GRS80();

        static internal double atan2(double y, double x)
        {
            return Math.Atan2(y, x);
        }

        static internal double sqrt(double x)
        {
            return Math.Sqrt(x);
        }

        static internal double cos(double x)
        {
            return Math.Cos(x);
        }

        static internal double sin(double x)
        {
            return Math.Sin(x);
        }

        static internal double square(double x)
        {
            return x * x;
        }

        static internal double epsilon2(double x, double y)
        {
            double x2 = square(x);
            double y2 = square(y);
            return ((x2 - y2) / y2);
        }


        public class Ellipsoid
        {
            double const_e2;
            double const_a;
            double const_b;
            double const_eps2; 

            public Ellipsoid(double e2, double a, double b, double eps2)
            {
                const_e2 = e2;
                const_a = a;
                const_b = b;
                const_eps2 = eps2;
            }

            public void ellips2cartesian(double phi, double labda, double h, out double x, out double y, out double z)
            {
                double phi_rad = phi * DEGREES_2_RADIANS;
                double labda_rad = labda * DEGREES_2_RADIANS;

                // Hulpvariabelen
                double sin_labda = sin(labda_rad);
                double cos_labda = cos(labda_rad);
                double sin_phi = sin(phi_rad);
                double cos_phi = cos(phi_rad);

                double noemer0 = 1.0 - const_e2 * sin_phi * sin_phi;
                double N = const_a / sqrt(noemer0);

                x = (N + h) * cos_phi * cos_labda;
                y = (N + h) * cos_phi * sin_labda;
                z = (N * (1.0 - const_e2) + h) * sin_phi;
            }

            // Terugtransformatie m.b.v. methode van Bowring
            public void cartesian2ellips(double x, double y, double z, out double phi, out double labda, out double h)
            {
                double r = sqrt(x * x + y * y);
                double theta_rad = atan2(z * const_a, r * const_b);
                double labda_rad = atan2(y, x);

                double sin_theta = sin(theta_rad);
                double cos_theta = cos(theta_rad);

                double phi_rad = atan2(z + const_eps2 * const_b * sin_theta * sin_theta * sin_theta, r - const_e2 * const_a * cos_theta * cos_theta * cos_theta);
                double cos_phi = cos(phi_rad);
                double sin_phi = sin(phi_rad);

                phi = phi_rad / DEGREES_2_RADIANS;
                labda = labda_rad / DEGREES_2_RADIANS;

                double noemer0 = 1.0 - const_e2 * sin_phi * sin_phi;
                double N = const_a / sqrt(noemer0);
                h = r / cos_phi - N;
            }
        }

        public class Bessel1841 : Ellipsoid
        {
            public Bessel1841() :
                base(Bessel_e2,
                    Bessel_a,
                    Bessel_b,
                    epsilon2(Bessel_a, Bessel_b))
            { }
        }

        public class GRS80 : Ellipsoid
        {
            public GRS80() :
                base(GRS80_e2,
                    GRS80_a,
                    GRS80_b,
                    epsilon2(GRS80_a, GRS80_b))
            { }
        }


        // Constanten van de datumshift
        const double const_tx = 565.04;     // m
        const double const_ty = 49.91;      // m
        const double const_tz = 465.84;     // m
        const double const_alpha = -1.9848e-6;  // rad
        const double const_beta = 1.7439e-6;  // rad
        const double const_gamma = -9.0587e-6;  // rad
        const double const_delta = 4.0772e-6;

        const double const_ttx = const_tx - (const_delta * const_tx - const_gamma * const_ty + const_beta * const_tz);
        const double const_tty = const_ty - (const_gamma * const_tx + const_delta * const_ty - const_alpha * const_tz);
        const double const_ttz = const_tz - (-const_beta * const_tx + const_alpha * const_ty + const_delta * const_tz);

        // Constanten voor de dubbelprojectie van Schreiber
        const double PHI_0 = 187762.178;
        const double LABDA_0 = 19395.500;
        const double X0 = 155000.0;
        const double Y0 = 463000.0;

        const double const_a01 = 3236.0331637;
        const double const_a20 = -32.5915821;
        const double const_a02 = -0.2472814;
        const double const_a21 = -0.8501341;
        const double const_a03 = -0.0655238;
        const double const_a22 = -0.0171137;
        const double const_a40 = 0.0052771;
        const double const_a23 = -0.0003859;
        const double const_a41 = 0.0003314;
        const double const_a04 = 0.0000371;
        const double const_a42 = 0.0000143;
        const double const_a24 = -0.0000090;

        const double const_b10 = 5261.3028966;
        const double const_b11 = 105.9780241;
        const double const_b12 = 2.4576469;
        const double const_b30 = -0.8192156;
        const double const_b31 = -0.0560092;
        const double const_b13 = 0.0560089;
        const double const_b32 = -0.0025614;
        const double const_b14 = 0.0012770;
        const double const_b50 = 0.0002574;
        const double const_b33 = -0.0000973;
        const double const_b51 = 0.0000293;
        const double const_b15 = 0.0000291;

        const double const_c01 = 190066.98903;
        const double const_c11 = -11830.85831;
        const double const_c21 = -114.19754;
        const double const_c03 = -32.38360;
        const double const_c31 = -2.34078;
        const double const_c13 = -0.60639;
        const double const_c23 = 0.15774;
        const double const_c41 = -0.04158;
        const double const_c05 = -0.00661;

        const double const_d10 = 309020.31810;
        const double const_d02 = 3638.36193;
        const double const_d12 = -157.95222;
        const double const_d20 = 72.97141;
        const double const_d30 = 59.79734;
        const double const_d22 = -6.43481;
        const double const_d04 = 0.09351;
        const double const_d32 = -0.07379;
        const double const_d14 = -0.05419;
        const double const_d40 = -0.03444;

        ////////////////////////////////////////////////////////////////////////
        //
        // Datumshift transformatie van de Bessel ellipsoide naar GRS80 (ETRS89)
        //
        // Input (x1, y1, z1) : Cartesische Bessel coordinaten
        // Output (x2, y2, z2): Cartesiche GRS80 coordinaten
        ////////////////////////////////////////////////////////////////////////
        static void datumShiftBesselETRS89(double x1, double y1, double z1, out double x2, out double y2, out double z2)
        {
            x2 = x1 + const_tx + const_delta * x1 - const_gamma * y1 + const_beta * z1;
            y2 = y1 + const_ty + const_gamma * x1 + const_delta * y1 - const_alpha * z1;
            z2 = z1 + const_tz - const_beta * x1 + const_alpha * y1 + const_delta * z1;
        }

        static void datumShiftETRS89Bessel(double x2, double y2, double z2, out double x1, out double y1, out double z1)
        {
            x1 = x2 - const_ttx - (const_delta * x2 - const_gamma * y2 + const_beta * z2);
            y1 = y2 - const_tty - (const_gamma * x2 + const_delta * y2 - const_alpha * z2);
            z1 = z2 - const_ttz - (-const_beta * x2 + const_alpha * y2 + const_delta * z2);
        }

        // Dubbelprojectie van Schreiber d.m.v. reeksontwikkeling
        static void Bessel2RD(double phi, double labda, out double X, out double Y)
        {
            double dphi = (phi * 3600.0 - PHI_0) * 1.0e-4;
            double dlabda = (labda * 3600.0 - LABDA_0) * 1.0e-4;

            double dX = 0.0;
            double dY = 0.0;

            double dphi2 = dphi * dphi;
            double dphi3 = dphi2 * dphi;
            double dphi4 = dphi3 * dphi;
            double dlabda2 = dlabda * dlabda;
            double dlabda3 = dlabda2 * dlabda;
            double dlabda4 = dlabda3 * dlabda;
            double dlabda5 = dlabda4 * dlabda;

            dX += const_c01 * dlabda;
            dX += const_c11 * dphi * dlabda;
            dX += const_c21 * dphi2 * dlabda;
            dX += const_c03 * dlabda3;
            dX += const_c31 * dphi3 * dlabda;
            dX += const_c13 * dphi * dlabda3;
            dX += const_c23 * dphi2 * dlabda3;
            dX += const_c41 * dphi4 * dlabda;
            dX += const_c05 * dlabda5;

            dY += const_d10 * dphi;
            dY += const_d20 * dphi2;
            dY += const_d02 * dlabda2;
            dY += const_d12 * dphi * dlabda2;
            dY += const_d30 * dphi3;
            dY += const_d22 * dphi2 * dlabda2;
            dY += const_d40 * dphi4;
            dY += const_d04 * dlabda4;
            dY += const_d32 * dphi3 * dlabda2;
            dY += const_d14 * dphi * dlabda4;

            X = X0 + dX;
            Y = Y0 + dY;
        }

        static void RD2Bessel(double X, double Y, out double phi, out double labda)
        {
            double dX = (X - X0) * 1.0e-5;
            double dY = (Y - Y0) * 1.0e-5;

            double dX2 = dX * dX;
            double dX3 = dX2 * dX;
            double dX4 = dX3 * dX;
            double dX5 = dX4 * dX;
            double dY2 = dY * dY;
            double dY3 = dY2 * dY;
            double dY4 = dY3 * dY;
            double dY5 = dY4 * dY;

            double dphi = 0.0;
            double dlabda = 0.0;

            dphi += const_a01 * dY;
            dphi += const_a20 * dX2;
            dphi += const_a02 * dY2;
            dphi += const_a21 * dX2 * dY;
            dphi += const_a03 * dY3;
            dphi += const_a40 * dX4;
            dphi += const_a22 * dX2 * dY2;
            dphi += const_a04 * dY4;
            dphi += const_a41 * dX4 * dY;
            dphi += const_a23 * dX2 * dY3;
            dphi += const_a42 * dX4 * dY2;
            dphi += const_a24 * dX2 * dY4;

            dlabda += const_b10 * dX;
            dlabda += const_b11 * dX * dY;
            dlabda += const_b30 * dX3;
            dlabda += const_b12 * dX * dY2;
            dlabda += const_b31 * dX3 * dY;
            dlabda += const_b13 * dX * dY3;
            dlabda += const_b50 * dX5;
            dlabda += const_b32 * dX3 * dY2;
            dlabda += const_b14 * dX * dY4;
            dlabda += const_b51 * dX5 * dY;
            dlabda += const_b33 * dX3 * dY3;
            dlabda += const_b15 * dX * dY5;

            phi = (PHI_0 + dphi) / 3600.0;
            labda = (LABDA_0 + dlabda) / 3600.0;
        }

        static void ETRS89_RD(double phi, double labda, out double X, out double Y)
        {
            double x_etrs89 = 0.0;
            double y_etrs89 = 0.0;
            double z_etrs89 = 0.0;
            double x_bessel = 0.0;
            double y_bessel = 0.0;
            double z_bessel = 0.0;
            double phi_bessel = 0.0;
            double labda_bessel = 0.0;
            double h_bessel = 0.0;

            grs80.ellips2cartesian(phi, labda, 0.0, out x_etrs89, out y_etrs89, out z_etrs89);
            datumShiftETRS89Bessel(x_etrs89, y_etrs89, z_etrs89, out x_bessel, out y_bessel, out z_bessel);
            bessel.cartesian2ellips(x_bessel, y_bessel, z_bessel, out phi_bessel, out labda_bessel, out h_bessel);
            Bessel2RD(phi_bessel, labda_bessel, out X, out Y);
        }

        static void RD_ETRS89(double X, double Y, out double phi, out double labda)
        {
            double x_etrs89 = 0.0;
            double y_etrs89 = 0.0;
            double z_etrs89 = 0.0;
            double x_bessel = 0.0;
            double y_bessel = 0.0;
            double z_bessel = 0.0;
            double phi_bessel = 0.0;
            double labda_bessel = 0.0;
            double h_bessel = 0.0;
            double h = 0.0;

            RD2Bessel(X, Y, out phi_bessel, out labda_bessel);
            bessel.ellips2cartesian(phi_bessel, labda_bessel, h_bessel, out x_bessel, out y_bessel, out z_bessel);
            datumShiftBesselETRS89(x_bessel, y_bessel, z_bessel, out x_etrs89, out y_etrs89, out z_etrs89);
            grs80.cartesian2ellips(x_etrs89, y_etrs89, z_etrs89, out phi, out labda, out h);
        }

        public static void RD2GPS(double x, double y, out double lat, out double lon)
        {
            RD_ETRS89(x, y, out lat, out lon);
        }

        public static void GPS2RD(double lat, double lon, out double X, out double Y)
        {
            ETRS89_RD(lat, lon, out X, out Y);
        }

        // OWN implementations

        public static double CalcTileSizeRD( int zoom )
        {
            return ((RDTileSizeTop*RDDefaultTileRes) / (1 << zoom));
        }


        // Assumes Pivot is set or default (Forum right most position)
        public static Vector3 ToRD( double lat, double lon )
        {
            GPS2RD( lat, lon, out double x, out double y );
            float fx = (float)(x - RDPivotX);
            float fy = (float)(y - RDPivotY);
            return new Vector3( fx, 0, fy );
        }

        public static Vector2Int RD2Tile( Vector2 rd, int zoom )
        {
            double tileSize = CalcTileSizeRD( zoom );
            double dx = rd.x - RDOriginX;
            double dy = rd.y - RDOriginY;
            int tx = Mathf.FloorToInt( (float)(dx/tileSize) );
            int ty = Mathf.FloorToInt( (float)(dy/tileSize) );
            ty = (1<<zoom) - ty - 1;
            return new Vector2Int( tx, ty );
        }

        // Returns lower left corner of tile in RD
        public static Vector2 Tile2RD( Vector2Int tile, int zoom )
        {
            double tileSize = CalcTileSizeRD( zoom );
            float x = (float)(tile.x * tileSize + RDOriginX);
            tile.y = (1<<zoom) - tile.y - 1;
            float y = (float)(tile.y * tileSize + RDOriginY);
            return new Vector2( x, y );
        }
    }
}