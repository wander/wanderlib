﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Wander
{
    public static class MathUtil
    {
        public static void ClampAngle360( ref float ang )
        {
            const float oneOver = 1.0f / 360;
            if (ang < 0) ang -= Mathf.FloorToInt( ang * oneOver ) * 360;
            if (ang > 360) ang -= Mathf.FloorToInt( ang * oneOver ) * 360;
        }


        public static int ClampAngle360Int( float ang )
        {
            ClampAngle360( ref ang );
            int iAng = Mathf.RoundToInt(ang);
            Debug.Assert( iAng >= 0 && iAng <= 360 );
            return iAng;
        }

        public static float DistSq( this Vector3 a, Vector3 b )
        {
            Vector3 diff = a-b;
            return Vector3.Dot( diff, diff );
        }

        // Function for life
        public static float Friction( float friction, float dt )
        {
            return Mathf.Clamp( Mathf.Exp( -friction * dt ), 0.0f, 1.0f );
        }

        public static float Smooth( float smooth, float dt )
        {
            return (1.0f - Friction( smooth, dt ));
        }

        public static void AnimateWithAccel( ref float speed, float accel, float maxSpeed, float friction, float dt, bool isAccelerating )
        {
            if (isAccelerating)
            {
                speed += accel*dt;
                if (speed > maxSpeed) speed = maxSpeed;
            }
            else
            {
                speed *= Friction( friction, dt );
            }
        }

        public static bool IsSane( this float f )
        {
            return f >= float.MinValue && f <= float.MaxValue;
        }

        public static bool IsSane( this Vector2 v )
        {
            return v.x.IsSane() && v.y.IsSane();
        }

        public static bool IsSane( this Vector3 v )
        {
            return v.x.IsSane() && v.y.IsSane() && v.z.IsSane();
        }

        public static bool IsSane( this double d )
        {
            return d >= double.MinValue && d <= double.MaxValue;
        }
    }
}