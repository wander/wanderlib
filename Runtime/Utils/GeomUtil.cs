﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Wander
{
    public static class GeomUtil
    {
        /*  Reduce resolution of a normalized vector. */
        public static Vector3 ReduceResolution(this Vector3 v, float resolution=32)
        {
            Debug.Assert( Mathf.Abs( Vector3.Dot( v, v )-1 )<0.001f ); // Assert normalized.
            float invRes = 1.0f / resolution;
            var n = v * resolution;
            n.x = Mathf.Round( n.x );
            n.y = Mathf.Round( n.y );
            n.z = Mathf.Round( n.z );
            return n*invRes;
        }

        public static Vector3 CrossWithUpVector( Vector3 v )
        {
            return new Vector3( -v.z, 0, v.x );
        }

        public static void GenerateUvs( Vector3 n, Vector3 v0, Vector3 v1, Vector3 v2, out Vector2 uv0, out Vector2 uv1, out Vector2 uv2 )
        {
            Vector3 u = CrossWithUpVector( n );
            Debug.Assert( u.IsSane(), "Vector contains invalid number" );

            if (Vector3.Dot( u, u ) < 0.01f)
                u = Vector3.right;
            else
                u = u.normalized;

            Debug.Assert( u.IsSane(), "Vector contains invalid number" );
            Vector3 v = (Vector3.Cross(n, u)).normalized;
            Debug.Assert( v.IsSane(), "Vector contains invalid number" );

            uv0 = new Vector2( Vector3.Dot( v0, u ), Vector3.Dot( v0, v ) );
            uv1 = new Vector2( Vector3.Dot( v1, u ), Vector3.Dot( v1, v ) );
            uv2 = new Vector2( Vector3.Dot( v2, u ), Vector3.Dot( v2, v ) );

            Debug.Assert( uv0.IsSane() && uv1.IsSane() && uv2.IsSane(), "Vector contains invalid number" );
        }

        // It works! Verified.
        public static bool IsPointInsideConvexHull( Vector3 point, List<Vector3> verts, List<int> tris )
        {
            for (int i = 0;i < tris.Count; i+=3)
            {
                var i0 = tris[i];
                var i1 = tris[i+1];
                var i2 = tris[i+2];

                Vector3 v1 = verts[i0];
                Vector3 v2 = verts[i1];
                Vector3 v3 = verts[i2];

                Vector3 triNormal = Vector3.Cross(v3 - v1, v2 - v1).normalized;

                float side = Vector3.Dot(point - v1, triNormal);

                if (side < 0)
                    return false;
            }

            return true;
        }


        // It works! Verified.
        public static bool IsBoundingBoxInsideConvexHull( List<Vector3> convexHull, Bounds boundingBox )
        {
            // The number of edges in the convex hull
            int edgeCount = convexHull.Count;

            // A variable to keep track of whether the bounding box is inside the convex hull or not
            bool isInside = true;

            for (int i = 0;i < edgeCount;i++)
            {
                // The current edge
                Vector3 edge = convexHull[(i + 1) % edgeCount] - convexHull[i];

                // The normal of the current edge
                Vector3 normal = Vector3.Cross(edge, Vector3.up).normalized;

                // The minimum and maximum values of the projection of the convex hull onto the current normal
                float minConvexHull = float.PositiveInfinity;
                float maxConvexHull = float.NegativeInfinity;

                // Project all the points of the convex hull onto the current normal
                for (int j = 0;j < edgeCount;j++)
                {
                    float projection = Vector3.Dot(convexHull[j], normal);
                    minConvexHull = Mathf.Min( minConvexHull, projection );
                    maxConvexHull = Mathf.Max( maxConvexHull, projection );
                }

                // The minimum and maximum values of the projection of the bounding box onto the current normal
                float minBoundingBox = float.PositiveInfinity;
                float maxBoundingBox = float.NegativeInfinity;

                // Project the 8 corners of the bounding box onto the current normal
                for (int j = 0;j < 8;j++)
                {
                    Vector3 corner = boundingBox.min + new Vector3(boundingBox.size.x * (j & 1), boundingBox.size.y * ((j >> 1) & 1), boundingBox.size.z * (j >> 2));
                    float projection = Vector3.Dot(corner, normal);
                    minBoundingBox = Mathf.Min( minBoundingBox, projection );
                    maxBoundingBox = Mathf.Max( maxBoundingBox, projection );
                }

                // If the maximum value of the convex hull projection is less than the minimum value of the bounding box projection or vice versa, then there is no intersection and the bounding box is not inside the convex hull.
                if (maxConvexHull < minBoundingBox || minConvexHull > maxBoundingBox)
                {
                    isInside = false;
                    break;
                }
            }
            return isInside;
        }

        // It Works! Computes volume of mesh (must be convex). 
        public static float CalcVolume( List<Vector3> verts, List<int> tris, List<Vector3> normals )
        {
            float volume = 0;
            for (int t = 0;t < tris.Count;t += 3)
            {
                var i0 = tris[t+0];
                var i1 = tris[t+1];
                var i2 = tris[t+2];
                Vector3 v0 = verts[i0];
                Vector3 v1 = verts[i1];
                Vector3 v2 = verts[i2];
                Vector3 cr = Vector3.Cross( (v1-v0), (v2-v0) );
                Vector3 n  = cr.normalized;
                float area = cr.magnitude * 0.5f;
                float h = -Vector3.Dot( verts[0] - v0, n );
                float v = (area * h)/3;
                volume += v;
            }
            return volume;
        }
    }
}