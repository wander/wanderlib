using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Wander;
using static Wander.Projections;

namespace LAZNamespace
{
    public enum CellGfxDataType
    {
        Mesh,
        Buffers
    }

    public enum CullingType
    {
        None,
        OnlyVisibility,
        VisibilityAndUnloadData
    }

    public enum CellDivideMethod
    {
        Linear,
        Quadratic2,
        Quadratic4,
        Quadratic8,
        Quadratic16,
    }

    public class GenericTask
    {
        // Input
        public Action callback;
        public Action onDone;
        // Output
        public bool done;
    }

    public class TraceLineQuery
    {
        public Vector3 start;
        public Vector3 dir;
        public float radius2;
        public int pointSkipStep;
        // Output
        public LAZVertex vertex;
        public bool hit;
        public float hitDistance;
        public bool done;
        public LAZRenderer renderer;
    }

    public class TraceVerticesQuery
    {
        public Vector3 centre;
        public float radius;
        public int pointsToProgress;
        // Output
        public List<Tuple<LAZRenderer, LAZOctreeCell, int>> indices;
        public bool done;
    }

    public class TraceHullQuery
    {
        public List<Vector3> points;
        public List<int> tris;
        public int pointsToProgress;
        // Output
        public List<Tuple<LAZRenderer, LAZOctreeCell, int>> indices;
        public bool done;
    }

    [ExecuteAlways()]
    public class LAZLoader : MonoBehaviour
    {
        [Header("Stats")]
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public string CurrentLoadingFile;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float PercentageCompleted;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public string filenameRelativeOctree;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public List<string> loadedOctrees;

        [Header("Visibility")]
        public CullingType cullingType;
        [Tooltip("Set to 0 or less to have no max")]
        public long renderPointBudget = 10000000;
        [Tooltip("Set to 0 or less to have no max")]
        public float maxViewDistance = 500;

        [Header("Tree setup")]
        [Tooltip("Changes the graphics data type of an octree cell. Changing this requires reloading all files.")]
        public CellGfxDataType cellGfxDataType = CellGfxDataType.Mesh;
        [Tooltip("If number of points in cell exceeds this, it splits itself.")]
        public int numPointsPerCell = 2000000;
        [Tooltip("Number of LODs per cell. Careful to not increase too high so that memory is preserved.")]
        public int numLODSPerCell = 4;
        [Tooltip("Max memory a point cloud can ocupy, if exceeding, points are skipped linearly to fit to max budget.")]
        public float maxMemorySizeGB = 4;
        [Tooltip("If true, the input files are voxelized and put into asset bundles.")]
        public bool streamData = true;
        [Tooltip("How LOD levels are computed. If linear and number of LOD levels is 4, then each new LOD is 25% less points. If Quadratic, then each subsequent LOD is halved or a quater (Quadratic4) etc.")]
        public CellDivideMethod divideMethod = CellDivideMethod.Quadratic8;
        [Tooltip("Number of tasks in taskpool to generate voxel data, too many may stall the main thread.")]
        public int numGenericTasks = 2;

        [Header("Projection")]
        [Tooltip("Camera to use for LOD calculations.")]
        public Camera cam;
        [Tooltip("Target output projection.")]
        public Projection targetProjection = Projection.NotSet;
        [Tooltip("Leave blank to use targetProject, otherwise attempt to convert WKT to projection is made.")]
        public string targetProjectionCustomWKT;

        public bool IsStopping => done;
        public LAZOctree RelativeOctree { get; private set; }
        public long ElapsedMilliseconds => stopwatch.ElapsedMilliseconds;

        bool done;
        string appDataPath;
        Vector3 camPosCopy;
        Plane [] frustumPlanes;
        Dictionary<string, LAZOctree> lazFiles;
        Dictionary<LAZRenderer, LAZOctree> renderers;
        List<GenericTask>[] genericTasks;
        Task setupRenderingTask;
        Task[] genericTask;
        int genericTaskIterator;
        System.Diagnostics.Stopwatch stopwatch;

        // Internal so that LAZOctreeCell can register itself as visible.
        internal List<Tuple<LAZRenderer, LAZOctreeCell, int, float>> [] visibleCells;
        internal int cellsRenderIdx = 0;
        internal int cellsFillIdx = 1;

        [Header("Debug")]
        public bool seeIfRendererTaskIsExecuting;

        internal string AppDataPath
        {
            get // First called from mainthread. So initialization should go fine.
            {
                if (appDataPath == null)
                    appDataPath = Application.dataPath;
                return appDataPath;
            }
        }

        #region Setup & Cleanup

        internal static void GetSingle( ref LAZLoader loader )
        {
            if (loader != null)
            {
                return;
            }
            var loaders = FindObjectsOfType<LAZLoader>();
            if (loaders != null && loaders.Length > 0)
            {
                loader = loaders[0];
                if (loaders.Length > 1)
                {
                    Debug.LogWarning( "Multiple LAZLoaders found in scene, selected the first." );
                }
            }
        }

        private void Awake()
        {
            if (cam == null)
            {
                cam = FindObjectOfType<Camera>();
            }
        }

        void OnEnable() => Startup();
        void OnDisable() => Shutdown();
        void OnDestroy() => Shutdown();

        void Startup()
        {
            /*nameFirstLoadedOctree = "";*/ // <- This is saved in scene, do not reset. This is to fetch back the relative octree when scene is loaded from disk.
            PercentageCompleted = 0;
            CurrentLoadingFile = "";
            done               = false;
            appDataPath        = Application.dataPath;
            frustumPlanes      = new Plane[6];
            loadedOctrees      = new List<string>();
            stopwatch          = new System.Diagnostics.Stopwatch();
            stopwatch.Restart();

            // ----- Only create these containers if not already from other function. ---
            // ----- This scripts runs from OnEnable() -> Startup, but OnEnable from other scripts might run first. ----
            {
                if (genericTasks == null)
                {
                    genericTasks = new List<GenericTask>[numGenericTasks];
                    for (int i = 0;i < numGenericTasks;i++)
                        genericTasks[i] = new List<GenericTask>();
                }
                if (lazFiles == null) lazFiles = new Dictionary<string, LAZOctree>();
                if (renderers == null) renderers = new Dictionary<LAZRenderer, LAZOctree>();

                visibleCells    = new List<Tuple<LAZRenderer, LAZOctreeCell, int, float>>[2];
                visibleCells[0] = new List<Tuple<LAZRenderer, LAZOctreeCell, int, float>>();
                visibleCells[1] = new List<Tuple<LAZRenderer, LAZOctreeCell, int, float>>();
            }

            // --- Tasks -----------
            setupRenderingTask  = Task.Run( RenderingTask );
            genericTask         = new Task[numGenericTasks];
            for (int i = 0;i <numGenericTasks;i++)
            {
                int intCpy = i;
                genericTask[i] = Task.Run( () => { GenericTasks( intCpy ); } );
            }
        }

        void Shutdown()
        {
            done = true;
            setupRenderingTask?.Wait();
            if (genericTask != null)
            {
                for (int i = 0;i <genericTasks.Length;i++)
                {
                    lock (genericTasks[i])
                    {
                        Monitor.Pulse( genericTasks[i] );
                    }
                    genericTask[i].Wait();
                }
            }

            /* TODO cleanup gfx data 
            if ( lazFiles != null )
            {
                foreach( var kvp in lazFiles )
                {
                    var tree = kvp.Value;
                    tree.Traverse( cb =>
                    {
                        if(  cb.gfxData != null )
                        {
                            cb.gfxData.ReleaseMT();
                        }
                    } );
                }
            } */
        }

        internal void ReloadFiles()
        {
            Shutdown();
            lazFiles = null;
            renderers = null;
            visibleCells = null;
            Startup();

            /// Do meta search in scene on all renderers and swap back and forth their enable status.
            var lazRenderers = FindObjectsOfType<LAZRenderer>();
            for (int i = 0;i< lazRenderers.Length;i++)
            {
                lazRenderers[i].enabled = !lazRenderers[i].enabled;
                lazRenderers[i].enabled = !lazRenderers[i].enabled;
            }
        }

        internal void SetRelativeTree( LAZOctree tree )
        {
            Debug.Assert( tree != null );
            RelativeOctree = tree;
            filenameRelativeOctree = tree.Filename;
        }

        #endregion

        #region Main thread loop (MT)

        internal void Update()
        {
            if (cam == null)
            {
                if (UnityEngine.Random.value < 0.1f)
                {
                    Debug.LogError( "No camera set, nothing will be streamed in! Assign a camera in LAZ Loader in 'Projection' section." );
                }
                return;
            }

            // Copy frustum planes on main thread
            lock (frustumPlanes)
            {
                GeometryUtility.CalculateFrustumPlanes( cam, frustumPlanes );
                camPosCopy = cam.transform.position;
            }

            // Render visible cells, they are sorted by distance.
            lock (visibleCells)
            {
                long renderedPoints = 0;
                for (int i = 0;i < visibleCells[cellsRenderIdx].Count;i++)
                {
                    var renderer = visibleCells[cellsRenderIdx][i].Item1;
                    var cell     = visibleCells[cellsRenderIdx][i].Item2;
                    int lod      = visibleCells[cellsRenderIdx][i].Item3;
                    float dist   = visibleCells[cellsRenderIdx][i].Item4;

                    // Quit if view distance exceeded.
                    if (maxViewDistance > 0 && dist > maxViewDistance)
                        break;

                    Debug.Assert( cell != null );
                    int cellRenderPoints = cell.GetNumIndices( lod, divideMethod );
                    renderer.RenderCell( cell, lod );

                    // Quit if point budget reached.
                    if (renderPointBudget > 0)
                    {
                        renderedPoints += cellRenderPoints;
                        if (renderedPoints > renderPointBudget)
                        {
                            break;
                        }
                    }
                }
            }
        }

        #endregion

        #region Register new Renderer/Octree

        internal LAZOctree RegisterRenderer( LAZRenderer renderer )
        {
            if (renderers == null)
            {
                renderers = new Dictionary<LAZRenderer, LAZOctree>();
            }
            var lazFile = AddLazFile( renderer );
            if (lazFile != null)
            {
                lock (renderers)
                {
                    if (!renderers.ContainsKey( renderer ))
                    {
                        renderers.Add( renderer, lazFile );
                    }
                }
                return lazFile;
            }
            return null;
        }

        internal void UnregisterRenderer( LAZRenderer renderer )
        {
            if (renderers == null)
                return;
            lock (renderers)
            {
                renderers.Remove( renderer );
            }
        }

        LAZOctree AddLazFile( LAZRenderer renderer )
        {
            Debug.Assert( renderer != null );
            if (!renderer.fileName.HasContent() ||
                 !renderer.relativePath.HasContent() ||
                 !renderer.cacheFolder.HasContent())
            {
                Debug.LogWarning( "Invalid filename, no content", renderer.gameObject );
                return null;
            }

            if (lazFiles == null)
            {
                lazFiles = new Dictionary<string, LAZOctree>();
            }

            // No need to put lock around this because other thread only reads from lazFiles. Only when we 'Add' here, we need a lock.
            if (lazFiles.TryGetValue( renderer.fileName, out LAZOctree file ))
            {
                return file;
            }

            // Create initial empty Octree for laz file.
            LAZOctree laz = new LAZOctree( this, renderer );

            // Try to fetch relative octree or set as first if no former set.
            if (RelativeOctree == null)
            {
                // Fetch from scene data.
                if (filenameRelativeOctree == laz.Filename && filenameRelativeOctree.HasContent())
                {
                    RelativeOctree = laz;
                }
                else if (!filenameRelativeOctree.HasContent())
                {
                    // No former relative tree set. Assign first one.
                    filenameRelativeOctree = laz.Filename;
                    RelativeOctree = laz;
                }
            }

            lock (lazFiles)
            {
                lazFiles.Add( renderer.fileName, laz );
            }
            loadedOctrees.Add( renderer.fileName ); // This is, so one can see in inspector which are ones are loaded.

            // Start the process of building or reading tree from disk.
            StartGenericTask( () =>
            {
                // See if tree is already on disk.
                if (!laz.ReadTreeFile())
                {
                    if (Path.GetExtension( laz.Filename ).ToLower() == ".txt")
                    {
                        laz.BuildFromAscii();
                    }
                    else
                    {
                        laz.ReadHeaderLaz();
                        laz.BuildLaz();
                    }
                    if (done) return; // Check cancel
                    if (streamData)
                    {
                        // Leaf is signed ready when its data is written to disk.
                        laz.WriteTreeFile();
                    }
                    else
                    {
                        // If not writing the data to disk, we sign leafs ready now.
                        laz.Traverse( ( cell, depth ) =>
                        {
                            if (cell.IsLeaf) cell.LeafReady=true;
                        } );
                    }
                    laz.VerifyAndGetStats();
                }
                else // Tree is on disk.
                {
                    laz.VerifyAndGetStats();
                    laz.Traverse( ( cell, depth ) => // Sign the leafs ready so that they can be loaded from disk.
                    {
                        if (cell.IsLeaf) cell.LeafReady=true;
                    } );
                }
            } );

            return laz;
        }

        #endregion

        #region Rendering

        void RenderingTask()
        {
            while (!done)
            {
                // Mark previously visible as invisible first
                for (int i = 0;i < visibleCells[cellsFillIdx].Count;i++)
                {
                    var cell = visibleCells[cellsFillIdx][i].Item2;
                    cell.visible = false; // TODO this will give a glits if queried, because it may be still visible, but is detected only in code below.
                }
                visibleCells[cellsFillIdx].Clear();

                // For each renderer: check each leaf and see if is visible, add the cell with correct LOD to list of visible cells.
                lock (renderers)
                {
                    foreach (var kvp in renderers)
                    {
                        var lazFile = kvp.Value;
                        if (lazFile != null && lazFile.IsReady)
                        {
                            var renderer = kvp.Key;
                            lazFile.ComputeLODPerCellPerRenderer( renderer, frustumPlanes, camPosCopy );
                        }
                    }
                }

                // If the loader loads files from cache, allow them to unload meshes when no longer visible.
                if (cullingType != CullingType.None)
                {
                    lock (lazFiles)
                    {
                        foreach (var kvp in lazFiles)
                        {
                            var tree = kvp.Value;
                            if (!tree.IsReady) continue;
                            tree.Root.UnloadCells();
                        }
                    }
                }

                // Sort on distance from cam, dist to cam.
                visibleCells[cellsFillIdx].Sort( ( a, b ) =>
                {
                    if (a.Item4 < b.Item4) return -1;
                    return 1;
                } );

                lock (visibleCells)
                {
                    cellsRenderIdx = (cellsRenderIdx + 1)&1;
                    cellsFillIdx = (cellsFillIdx + 1)&1;
                }

                Thread.Sleep( 50 );
                if (seeIfRendererTaskIsExecuting)
                {
                    Debug.Log( "Render executing" );
                }
            }
        }

        #endregion

        #region Tracing

        public TraceHullQuery StartHullQuery( List<Vector3> points, List<int> tris, int pointsToProgress = 1 )
        {
            Debug.Assert( pointsToProgress >= 1 ); // Otherwise possible infinite loop.

            TraceHullQuery hq = new TraceHullQuery();
            hq.done = false;
            hq.pointsToProgress = pointsToProgress;
            hq.points = points;
            hq.tris = tris;
            hq.indices = new List<Tuple<LAZRenderer, LAZOctreeCell, int>>();

            StartGenericTask( () =>
            {
                ProcessHullQuery( hq );
            } );

            return hq;
        }

        public TraceVerticesQuery StartVertexQuery( Vector3 position, float radius, int pointsToProgress = 1 )
        {
            Debug.Assert( pointsToProgress >= 1 ); // Otherwise possible infinite loop.

            TraceVerticesQuery vq = new TraceVerticesQuery();
            vq.done = false;
            vq.centre = position;
            vq.radius = radius;
            vq.pointsToProgress = pointsToProgress;
            vq.indices = new List<Tuple<LAZRenderer, LAZOctreeCell, int>>();

            StartGenericTask( () => 
            {
                ProcessVertexQuery( vq );
            } );

            return vq;
        }

        public TraceLineQuery StartLineTrace( Vector3 start, Vector3 dir, int pointsToProgress = 50, float radius = 0.1f, float distance = 100 )
        {
            TraceLineQuery tlq = new TraceLineQuery();
            tlq.done = false;
            tlq.start = start;
            tlq.dir = dir;
            tlq.radius2 = radius*radius;
            tlq.hitDistance = distance;
            tlq.pointSkipStep = pointsToProgress;

            Debug.Assert( pointsToProgress >= 1 );

            StartGenericTask( () =>
            {
                ProcessLineQuery( tlq );
            } );

            return tlq;
        }

        void ProcessHullQuery( TraceHullQuery hq )
        {
            try
            {
                lock (renderers)
                {
                    foreach (var kvpr in renderers)
                    {
                        var renderer = kvpr.Key;
                        var lazFile  = kvpr.Value;
                        if (!lazFile.IsReady)
                            continue;
                        lazFile.TraceHull( renderer, hq );
                    }
                }
            }
            finally
            {
                Thread.MemoryBarrier();
                hq.done = true;
            }
        }

        void ProcessVertexQuery( TraceVerticesQuery vq )
        {
            try
            {
                lock (renderers)
                {
                    foreach (var kvpr in renderers)
                    {
                        var renderer = kvpr.Key;
                        var lazFile  = kvpr.Value;
                        if (!lazFile.IsReady)
                            continue;
                        lazFile.TraceVertices( renderer, vq );
                    }
                }
            }
            finally
            {
                Thread.MemoryBarrier();
                vq.done = true;
            }
        }

        void ProcessLineQuery( TraceLineQuery q )
        {
            try
            {
                lock (renderers)
                {
                    foreach (var kvpr in renderers)
                    {
                        var renderer = kvpr.Key;
                        var lazFile  = kvpr.Value;
                        if (!lazFile.IsReady)
                            continue;
                        lazFile.TraceClosestPoint( renderer, q );
                    }
                }
                q.hit = q.renderer != null;
                if (q.hit)
                {
                    Debug.Assert( q.renderer );
                    q.renderer.GetWorldTransform( out Matrix4x4 worldTransform );
                    q.vertex.position = worldTransform.MultiplyPoint( q.vertex.position );
                }
            }
            finally
            {
                Thread.MemoryBarrier();
                q.done = true;
            }
        }

        #endregion

        #region Generic Tasks
        public GenericTask StartGenericTask( Action callback, Action onDone=null, bool executeOnMain=false )
        {
            // Lazy initialize, if not already on main thread.
            if (genericTasks == null)
            {
                genericTasks = new List<GenericTask>[numGenericTasks];
                for (int i = 0;i < numGenericTasks;i++)
                    genericTasks[i] = new List<GenericTask>();
            }

            GenericTask gt = new GenericTask();
            gt.callback = callback;
            gt.onDone = onDone;
            gt.done = false;
            uint taskIdx = (uint) genericTaskIterator++;
            taskIdx %= (uint)numGenericTasks;
            if (executeOnMain)
                taskIdx = 0; // This allows ordered execution of tasks.
            lock (genericTasks[taskIdx])
            {
                genericTasks[taskIdx].Add( gt );
                Monitor.Pulse( genericTasks[taskIdx] );
            }
            return gt;
        }

        void GenericTasks( int taskIdx )
        {
            while (!done)
            {
                GenericTask gt = null;
                lock (genericTasks[taskIdx])
                {
                    if (genericTasks[taskIdx].Count > 0)
                    {
                        gt = genericTasks[taskIdx][0];
                        genericTasks[taskIdx].RemoveAt( 0 );
                    }
                    if (!done && gt == null)
                    {
                        Monitor.Wait( genericTasks[taskIdx] );
                    }
                }
                if (gt != null)
                {
                    try
                    {
                        gt.callback();
                        Thread.MemoryBarrier();
                    }
                    finally
                    {
                        gt.done = true;
                        if (gt.onDone!=null) gt.onDone();
                    }
                }
            }
        }

        #endregion
    }

    #region UnityInspector

#if UNITY_EDITOR
    [CustomEditor( typeof( LAZLoader ) )]
    [InitializeOnLoad]
    public class LAZStreamerEditor : Editor
    {
        static LAZStreamerEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            LAZLoader streamer = (LAZLoader)target;

            EditorGUILayout.HelpBox( "If the 'Tree setup' section is changed, cached meshes may not load back correctly, because the tree structure is not cached to disk. Always clear cache if altering the 'Tree setup'. This is not needed if 'Cache Meshes To Scene' is unticked.", MessageType.Warning, true );


            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Reload files" ))
                {
                    streamer.ReloadFiles();
                }
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();
        }
    }

#endif

    #endregion
}