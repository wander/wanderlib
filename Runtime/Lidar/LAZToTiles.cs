using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace LAZNamespace
{
    [ExecuteInEditMode()]
    internal class LAZToTiles : MonoBehaviour
    {
        [Tooltip("LIDAR folder to load. All files in folder will be converted into tiles.")]
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public string folder;

        private void Awake()
        {
        }

        internal void Generate()
        {
            if (string.IsNullOrWhiteSpace( folder ))
            {
                UnityEngine.Debug.LogWarning( "Invalid folder" );
                return;
            }

            string cd = Directory.GetCurrentDirectory();
            print( cd );

            Process.Start( "lastile.exe", "-i " );
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( LAZToTiles ) )]
    [InitializeOnLoad]
    public class LAZToTilesEditor : Editor
    {
        static LAZToTilesEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            LAZToTiles lazToTiles = (LAZToTiles)target;

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Select folder" ))
                {
                    string s = EditorUtility.OpenFolderPanel( lazToTiles.folder, "", "" );
                    s = s.Trim();
                    if (!string.IsNullOrEmpty( s ))
                    {
                        lazToTiles.folder = s;
                    }
                    else
                    {
                        UnityEngine.Debug.LogWarning( "Invalid folder" );
                    }
                }

                if (GUILayout.Button( "Generate tiles" ))
                {
                    lazToTiles.Generate();
                }
            }
            GUILayout.EndHorizontal();

            //   EditorGUILayout.HelpBox( "File must be in StreamingAssets folder or subdirectory thereof.", MessageType.Warning );

            DrawDefaultInspector();

            Repaint();
        }
    }
#endif

}