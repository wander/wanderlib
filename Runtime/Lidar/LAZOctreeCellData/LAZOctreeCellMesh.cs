﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using Wander;

namespace LAZNamespace
{
    public interface LAZOctreeCellGfxData
    {
        public void MarkDirty();
        internal void UnloadDynamic();
    }

    internal class LAZOctreeCellMesh : LAZOctreeCellGfxData
    {
        Mesh mesh; // Each lod level has its own submesh. 
        Mesh.MeshDataArray meshDataArray;
        LAZOctree tree;
        LAZOctreeCell cell;
        GenericTask loadVoxelTask;
        bool meshUnloaded = true;
        bool meshDataDisposed = true;
        bool verticesDirty;

        internal string AssetBundleName { get; set; }

        public void MarkDirty()
        {
            verticesDirty = true;
        }

        public Mesh Mesh
        {
            get
            {
                if ((verticesDirty || meshUnloaded))// && cell.LeafReady /* <-- This is need to avoid that we start reading the file while still writing data to it, file sharing exception then occurs. */)
                {
                    if (cell.IsVisible || tree.Loader.cullingType == CullingType.None)
                    {
                        if (loadVoxelTask == null)
                        {
                            verticesDirty = false;
                            bool mustReload = tree.Loader.streamData && meshUnloaded;
                            AllocateMeshData();
                            loadVoxelTask = tree.Loader.StartGenericTask( () =>
                            {
                                if (mustReload)
                                {
                                    //   UnityEngine.Debug.Assert( cell.vertices == null, "Must be null vertices when streaming" ); // If streaming, this should have been nulled.
                                    cell.ReadLeafData();
                                }
                                CreateMeshData();
                            } );
                        }
                    }
                }
                // Regardless of visible, we must finish the task, otherewise it will only due when visible, but task was already started.
                if (loadVoxelTask != null && loadVoxelTask.done)
                {
                    loadVoxelTask = null;
                    FinalizeMeshData();
                    UnityEngine.Debug.Log( "done updating mesh" );
                }
                return mesh;
            }
        }

        internal LAZOctreeCellMesh(LAZOctreeCell _cell)
        {
            System.Diagnostics.Debug.Assert( cell != null );
            cell = _cell;
            tree = _cell.Tree;
            meshUnloaded = true;
        }

        /// --- Helper Functions ----------------------------------------------------------------------------------------------------------

        void DeallocateMeshData()
        {
            if (!meshDataDisposed)
            {
                meshDataArray.Dispose();
                meshDataDisposed = true;
            }
        }

        void AllocateMeshData()
        {
            meshDataArray = Mesh.AllocateWritableMeshData( 1 ); /// Cannot be executed in other thread.
            meshDataDisposed = false;
        }

        void CreateMeshData()
        {
            if (cell.vertices == null)
            {
                UnityEngine.Debug.LogWarning( "No Vertices loaded for cell. This happens when no cache data is created but 'StreamData' is turned on. Clear cache and Reload files." );
                return; // This can happen if cached file on disk is removed.
            }

            // ---- Setup vertices ----
            var vertexLayout = new[] // Layout must be multiple of 4
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),  /* cannot acces position.w in shader graph, so store intensity seperately */
                new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.UNorm16, 4),     /* color.a contains classification */
                new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, 1), /* texcoord0.r contains intensity */
            };
            Mesh.MeshData meshData = meshDataArray[0];
            meshData.SetVertexBufferParams( cell.vertices.Count, vertexLayout );
            NativeArray<LAZVertex> vertices = meshData.GetVertexData<LAZVertex>();
            vertices.CopyFrom( cell.vertices.ToArray() );

            // ---- Setup indices ----
            cell.CalcIndicesPerLODAndTotal(out int totalIndices, out int[] indicesPerLod );
            meshData.SetIndexBufferParams( totalIndices, IndexFormat.UInt32 );
            meshData.subMeshCount = tree.NumLodsPerCell;
            NativeArray<uint> indices = meshData.GetIndexData<uint>();
            int indexOffset = 0;
            for (int i = 0;i <tree.NumLodsPerCell;i++)
            {
                int numIndices = indicesPerLod[i];
                float curIdx   = 0;
                int vertCount  = cell.vertices.Count;
                float step     = cell.vertices.Count / (float)numIndices;
                for (int j = 0;j < numIndices; j++)
                {
                    uint index = (uint)Mathf.RoundToInt( curIdx );
                    if (index >= vertCount) index = (uint)(vertCount-1);
                    indices[indexOffset++] = index;
                    curIdx += step;
                }
            }
            System.Diagnostics.Debug.Assert( indexOffset == totalIndices );
            indexOffset = 0;
            for (int i = 0;i <tree.NumLodsPerCell;i++)
            {
                int numIndices = indicesPerLod[i];
                meshData.SetSubMesh( i, new SubMeshDescriptor( indexOffset, numIndices, MeshTopology.Points ) );
                indexOffset += numIndices;
            }
            System.Diagnostics.Debug.Assert( indexOffset == totalIndices );
        }

        void FinalizeMeshData()
        {
            // Verified flags combination wiht RecalculateBounds afterwards. RecalculateBounds is neeeded, otherwise everything is clipped.
            // However, enabling it in the flags wont work.
            var newMesh = new Mesh();
            var flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontValidateIndices;
            Mesh.ApplyAndDisposeWritableMeshData( meshDataArray, newMesh, flags ); // Cannot be executed on other thread.
            newMesh.RecalculateBounds();
            meshDataDisposed = true;
            mesh = newMesh;
            Thread.MemoryBarrier();
            meshUnloaded = false;
        }

        void LAZOctreeCellGfxData.UnloadDynamic()
        {
            if ( cell.LeafReady && !meshUnloaded )
            {
                if (tree.Loader.streamData)
                {
                    // Vertices will load back from disk.
                    cell.vertices = null;
                }  // else: Only unload mesh, keep vertices in memory as their is no disk equivalent.
                mesh = null;
                Thread.MemoryBarrier();
                meshUnloaded = true;
            }
        }
    }
}