#define LAZ_USE_BUFFERS

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;
using Wander;
using Debug = System.Diagnostics.Debug;


namespace LAZNamespace
{
    public class LAZOctreeCell
    {
        LAZOctree tree;
        Bounds bounds;
        Vector3 halfSize;
        float radius;
        int leafIdx=-1;
        LAZOctreeCell [] cells;
        long lastTimeVisible;
        int numPoints;

        internal bool visible;
        internal List<LAZVertex> vertices;
        internal LAZOctreeCellGfxData gfxData;

        internal LAZOctree Tree => tree;
        public bool LeafReady { get; internal set; }
        internal bool IsVisible => visible;
        internal int NumPoints => numPoints;
        internal long TimeSinceLastVisible => (tree.Loader.ElapsedMilliseconds - lastTimeVisible);
        public bool IsLeaf => (cells == null);
        public int LeafIndex => leafIdx;
        public Vector3 Min => bounds.min;
        public Vector3 Max => bounds.max;
        public Vector3 Centre => bounds.center;
        public Vector3 HalfSize => halfSize;
        public float Radius => radius;

        internal LAZOctreeCell( LAZOctree _tree )
        {
            tree = _tree;
        }

        // Causes recreation of Gfx data.
        public void MarkDirty()
        {
            gfxData?.MarkDirty();
        }

        internal void InitPhysicalParams( Vector3 _min, Vector3 _max )
        {
            bounds   = new Bounds( (_max+_min)*0.5f, (_max-_min) );
            halfSize = (_max-_min)*0.5f;
            radius   = halfSize.magnitude;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        string GetPathOnDisk()
        {
            Debug.Assert( LeafIndex >= 0 );
            return Path.Combine( tree.PathToCacheFolder, Path.GetFileNameWithoutExtension( tree.Filename ) + "_" + LeafIndex + ".voxel" );
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void AddVertex( LAZVertex vertex )
        {
            vertices.Add( vertex );
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal bool Contains( Vector3 point )
        {
            return !(point.x < bounds.min.x || point.x > bounds.max.x || point.y < bounds.min.y || point.y > bounds.max.y || point.z < bounds.min.z || point.z > bounds.max.z);
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        static internal bool Contains( Vector3 point, Vector3 min, Vector3 max )
        {
            return !(point.x < min.x || point.x > max.x || point.y < min.y || point.y > max.y || point.z < min.z || point.z > max.z);
        }

        internal void SplitAndFlush()
        {
            GetMinMaxes( out Vector3[] minN, out Vector3[] maxN );
            if ( cells == null) // We flush multiple times, so this is only null the first time.
            {
                cells = new LAZOctreeCell[8];
            }
            int k =0; // cache the choice
            for (int j = 0;j < vertices.Count;j++)
            {
                for (int i = 0;i < 8;i++)
                {
                    if (Contains( vertices[j].position, minN[k], maxN[k] ))
                    {
                        if (cells[k] == null)
                        {
                            cells[k] = new LAZOctreeCell( Tree );
                            cells[k].InitPhysicalParams( minN[k], maxN[k] );
                            cells[k].vertices = new List<LAZVertex>();
                        }
                        cells[k].AddVertex( vertices[j] );
                        break;
                    }
                    k++;
                    if (k==8) k=0;
                }
            }
            vertices.Clear();
            for (int i = 0;i < cells.Length;i++)
            {
                if (cells[i] != null)
                {
                    if (cells[i].vertices.Count > tree.NumPointsPerCell)
                    {
                        cells[i].SplitAndFlush();
                    }
                }
            }
        }

        internal void FlushOnly()
        {
            if (cells==null)
                return;
            GetMinMaxes( out Vector3[] minN, out Vector3[] maxN );
            int k =0; // cache the choice
            for (int j = 0;j < vertices.Count;j++)
            {
                for (int i = 0;i < 8;i++)
                {
                    if (Contains( vertices[j].position, minN[k], maxN[k] ))
                    {
                        cells[k]?.AddVertex( vertices[j] );
                        break;
                    }
                    k++;
                    if (k==8) k=0;
                }
            }
            vertices.Clear();
            for (int i = 0;i < cells.Length;i++)
            {
               cells[i]?.SplitAndFlush();
            }
        }

        internal void RemoveEmptyCells()
        {
            if (cells == null)
                return;
            int numValid = 0;
            for (int i = 0;i < cells.Length;i++)
            {
                if (cells[i] != null)
                    numValid++;
            }
            if (numValid != 0)
            {
                var newCells = new LAZOctreeCell[numValid];
                int j = 0;
                for (int i = 0;i < numValid;i++)
                {
                    do
                    {
                        if (cells[j] != null)
                        {
                            newCells[i] = cells[j];
                            ++j;
                            break;
                        }
                    } while (++j < 8);
                }
                Debug.Assert( j == numValid );
                cells = newCells;
            }
            else // (Edge case) Due to splitting, point might be just on edge or beyond, no cells found anymore.
            {
                cells = new LAZOctreeCell[0];
            }
            // Do children
            for (int i = 0;i<cells.Length;i++)
            {
                cells[i].RemoveEmptyCells();
            }
        }

        internal void Finalize( ref int leafIterator )
        {
            if (cells == null)
            {
                Debug.Assert( leafIdx == -1 );
                leafIdx = leafIterator++;
                return;
            }
            Debug.Assert( vertices.Count == 0 ); // Must have been flushed down the tree.
            vertices = null;
            for (int i = 0;i < cells.Length;i++)
            {
                cells[i].Finalize( ref leafIterator );
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        void GetMinMaxes( out Vector3[] minN, out Vector3[] maxN )
        {
            Vector3 hs = halfSize;
            minN = new Vector3[8];
            maxN = new Vector3[8];
            /// --------------------------------------
            minN[0] = bounds.min;
            maxN[0] = minN[0] + hs;
            minN[1] = bounds.min + new Vector3( hs.x, 0, 0 );
            maxN[1] = minN[1] + hs;
            minN[2] = bounds.min + new Vector3( hs.x, 0, hs.z );
            maxN[2] = minN[2] + hs;
            minN[3] = bounds.min + new Vector3( 0, 0, hs.z );
            maxN[3] = minN[3] + hs;
            /// --------------------------------------
            minN[4] = bounds.min + new Vector3( 0, hs.y, 0 );
            maxN[4] = minN[4] + hs;
            minN[5] = bounds.min + new Vector3( hs.x, hs.y, 0 );
            maxN[5] = minN[5] + hs;
            minN[6] = bounds.min + new Vector3( hs.x, hs.y, hs.z );
            maxN[6] = minN[6] + hs;
            minN[7] = bounds.min + new Vector3( 0, hs.y, hs.z );
            maxN[7] = minN[7] + hs;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        bool IsOutside( Plane p, Vector3 c, float r )
        {
            var p0  = p.normal*-p.distance; // point on plane
            var c0  = c - p0; // dir from point on plane to center
            float d = Vector3.Dot( c0, p.normal ) + r;
            return d < 0;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal bool IsSphereVisible( Plane[] frustumPlanes, ref Matrix4x4 worldTransform, Vector3 centreWorld )
        {
            for (int i = 0;i < 6;i++)
            {
                if (IsOutside( frustumPlanes[i], centreWorld, radius*1.3f ))
                    return false;
            }
            return true;
        }

        internal void ComputeAndSubscribePerCellInterest( LAZRenderer renderer, Plane[] frustumPlanes, Vector3 camOrigin, ref Matrix4x4 worldTransform )
        {
            var centreWorld = worldTransform.MultiplyPoint( Centre );
            if (tree.Loader.cullingType == CullingType.None ||
                IsSphereVisible( frustumPlanes, ref worldTransform, centreWorld ))
            {
                if (cells == null)
                {
                    //if (LeafReady)
                    {
                        float dist = Vector3.Distance( camOrigin, bounds.ClosestPoint( camOrigin ) );
                        //float dist = Vector3.Distance( camOrigin, centreWorld );

                        if (tree.Loader.maxViewDistance >0 && dist < tree.Loader.maxViewDistance)
                        {
                            lastTimeVisible = tree.Loader.ElapsedMilliseconds;
                            visible = true;

                            // Compute LOD
                            int lod = 0;

                            // TODO this compuation needs a serious check
                            {
                                var lodIndex         = dist / renderer.LODDistance;
                                float normalizedDist = Mathf.Clamp01( lodIndex / renderer.loader.numLODSPerCell );
                                normalizedDist       = (float)Math.Pow( normalizedDist, 1/renderer.LODLogaritmicFalloff );
                                lod                  = Mathf.RoundToInt( normalizedDist * (tree.NumLodsPerCell-1) );
                            }

                            // This renderer is interested, add to list.
                            tree.Loader.visibleCells[tree.Loader.cellsFillIdx].Add( new( renderer, this, lod, dist ) );
                        }
                    }
                }
                else
                {
                    for (int i = 0;i < cells.Length;i++)
                    {
                        cells[i].ComputeAndSubscribePerCellInterest( renderer, frustumPlanes, camOrigin, ref worldTransform );
                    }
                }
            }
        }

        internal void UnloadCells()
        {
            if (cells == null)
            {
                if (gfxData != null &&
                    (tree.Loader.ElapsedMilliseconds - lastTimeVisible > 5000))
                {
                    gfxData.UnloadDynamic();
                }
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i].UnloadCells();
                }
            }
        }

        public void ApplyChangeToVertices( Action<List<LAZVertex>> cb )
        {
            // Slice of as vertices may be unloaded after assignment.
            var slicedVertces = vertices;
            if (slicedVertces != null && slicedVertces.Count > 0)
            {
                cb( slicedVertces );
            }
        }

        internal void TraceHull(ref List<Tuple<LAZRenderer, LAZOctreeCell, int>> indices, List<Vector3> verts, List<int> tris, int pointsToProgress, LAZRenderer renderer)
        {
            // Note: points are in local space as is the bounds.
        //    if (!GeomUtil.IsBoundingBoxInsideConvexHull( verts, bounds ))
      //          return;

            if ( cells == null ) // Is leaf
            {
                // NOTE: vertices might be unloaded from different thread, but if ptr is valid after asignment, it cannot become NULL in C#.
                // So, memory is valid. The count may also change but can only increase, so it is valid to check .Count property.
                var collisionVertices = vertices;

                if (collisionVertices == null)
                    return;

                for (int i = 0; i < collisionVertices.Count; i++ )
                {
                    if (  GeomUtil.IsPointInsideConvexHull( collisionVertices[i].position, verts, tris ) )
                    {
                        indices.Add( new( renderer, this, i ) );
                    }
                }
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i].TraceHull( ref indices, verts, tris, pointsToProgress, renderer );
                }
            }
        }

        internal void TraceVertices( ref List<Tuple<LAZRenderer, LAZOctreeCell, int>> indices, Vector3 centreInvWorld, float _radius, int pointsToProgress, LAZRenderer renderer)
        {
            Vector3 diff = bounds.center - centreInvWorld;
            float distSq = Vector3.Dot( diff, diff );
            float rBoth  = (radius + _radius);
            
            // Out of range (whole cell and its children can be skipped).
            if (distSq > rBoth*rBoth)
                return;

            if ( cells == null ) // Is leaf
            {
                // NOTE: vertices might be unloaded from different thread, but if ptr is valid after asignment, it cannot become NULL in C#.
                // So, memory is valid. The count may also change but can only increase, so it is valid to check .Count property.
                var collisionVertices = vertices;

                if (collisionVertices == null)
                    return;

                var incomeRadiusSq = _radius*_radius;
                for (int i = 0;i < collisionVertices.Count;i += pointsToProgress)
                {
                    Vector3 point = collisionVertices[i].position;
                    diff = point - centreInvWorld;
                    distSq = Vector3.Dot( diff, diff );
                    if ( distSq < incomeRadiusSq )
                    {
                        indices.Add( new( renderer, this, i ) );
                    }
                }
            }
            else
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i].TraceVertices( ref indices, centreInvWorld, _radius, pointsToProgress, renderer );
                }
            }
        }

        internal void TraceClosestPoint( Ray rayInvTransformWorld, TraceLineQuery q, LAZRenderer renderer )
        {
            if (bounds.IntersectRay( rayInvTransformWorld, out _ ))
            {
                if (cells == null)
                {
                    // NOTE: vertices might be unloaded from different thread, but if ptr is valid after asignment, it cannot become NULL in C#.
                    // So, memory is valid. The count may also change but can only increase, so it is valid to check .Count property.
                    var collisionVertices = vertices;

                    if (collisionVertices == null)
                        return; // originalVertices may be NULL here, but collisionVertices will remain pointing to valid memory.

                    for (int i = 0;i < collisionVertices.Count;i += q.pointSkipStep)
                    {
                        Vector3 point = collisionVertices[i].position;
                        Vector3 pointToRayOrigin = point - rayInvTransformWorld.origin;
                        float newDist = Vector3.Dot(rayInvTransformWorld.direction, pointToRayOrigin);
                        if (newDist >= 0 && newDist < q.hitDistance) /// otherwise point is behind origin
                        {
                            Vector3 dirToLine = Vector3.Cross( rayInvTransformWorld.direction, pointToRayOrigin );
                            float distSq = Vector3.Dot(dirToLine, dirToLine);
                            if (distSq < q.radius2)
                            {
                                q.hitDistance = newDist;
                                q.vertex = collisionVertices[i];
                                q.renderer = renderer;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0;i < cells.Length;i++)
                    {
                        cells[i].TraceClosestPoint( rayInvTransformWorld, q, renderer );
                    }
                }
            }
        }

        internal void Write( BinaryWriter bw )
        {
            bw.Write( Min.x );
            bw.Write( Min.y );
            bw.Write( Min.z );
            bw.Write( Max.x );
            bw.Write( Max.y );
            bw.Write( Max.z );
            if (cells != null)
            {
                Debug.Assert( leafIdx >= 0 );
                bw.Write( (byte)cells.Length );
                for (int i = 0;i<cells.Length;i++)
                {
                    cells[i].Write( bw );
                }
            }
            else
            {
                bw.Write( (byte)0 );
                bw.Write( leafIdx );
            }
        }

        internal void Read( BinaryReader br )
        {
            float minx = br.ReadSingle();
            float miny = br.ReadSingle();
            float minz = br.ReadSingle();
            float maxx = br.ReadSingle();
            float maxy = br.ReadSingle();
            float maxz = br.ReadSingle();
            InitPhysicalParams( new Vector3( minx, miny, minz ), new Vector3( maxx, maxy, maxz ) );
            byte numCells = br.ReadByte();
            if (numCells != 0)
            {
                cells = new LAZOctreeCell[numCells];
                for (var i = 0;i< numCells;i++)
                {
                    cells[i] = new LAZOctreeCell( Tree );
                    cells[i].Read( br );
                }
            }
            else
            {
                leafIdx  = br.ReadInt32();
                Debug.Assert( leafIdx>=0 );
            }
        }

        public void WriteLeafData()
        {
            Debug.Assert( IsLeaf, "Is Leaf failure" );
            try
            {
                var verticesSliced = vertices;
                if (verticesSliced== null) // Might be unloading the data.
                    return;
                Debug.Assert( Directory.Exists( tree.PathToCacheFolder ) );
                var filePath = GetPathOnDisk().NormalizePath();
                Monitor.Enter( this );
                try
                {
                    using (var file = File.Open( filePath, FileMode.Create, FileAccess.Write ))
                    using (var br = new BinaryWriter( file ))
                    {
                        // Vertices
                        var count = verticesSliced.Count;
                        br.Write( count );
                        for (int i = 0;i < count;i++)
                        {
                            verticesSliced[i].Write( br );
                        }
                        br.Flush();
                    }
                }
                finally
                {
                    Monitor.Exit( this );
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException( e );
            }
        }

        internal void ReadLeafData()
        {
            Debug.Assert( IsLeaf, "Is Leaf failure" );
            try
            {
                // Check directory
                Debug.Assert( Directory.Exists( tree.PathToCacheFolder ) );

                // Check file
                var filePath = GetPathOnDisk();
                if (!File.Exists( filePath ))
                {
                    //   UnityEngine.Debug.LogWarning( "File: " + filePath + " in cache is missing, this is not an error but result is invalid. Please rebuild 'Clear Cache' and 'Reload files'." );
                    return;
                }

                // Read vertices
                Monitor.Enter( this );
                try
                {
                    using (var file = File.Open( filePath, FileMode.Open, FileAccess.Read, FileShare.Read ))
                    using (var br = new BinaryReader( file ))
                    {
                        // Vertices
                        int vertCount = br.ReadInt32();
                        var verticesNew = new List<LAZVertex>( vertCount );
                        LAZVertex ver = new LAZVertex();
                        for (int i = 0;i < vertCount;i++)
                        {
                            LAZVertex.Read( br, ref ver );
                            verticesNew.Add( ver );
                        }
                        numPoints = verticesNew.Count;
                        Debug.Assert( verticesNew.Count==vertCount, "Cache invalid. Clear cache and Reload files." );
                        vertices = verticesNew;
                    }
                }
                finally
                {
                    Monitor.Exit( this );
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException( e );
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CalcIndicesPerLODAndTotal( out int totalIndices, out int[] indicesPerLod )
        {
            totalIndices = 0;
            indicesPerLod = new int[tree.NumLodsPerCell];
            for (int i = 0;i <tree.NumLodsPerCell;i++)
            {
                indicesPerLod[i] = GetNumIndices( i, tree.DivideMethod );
                totalIndices += indicesPerLod[i];
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int GetNumIndices( int lod, CellDivideMethod divideMethod )
        {
            int numIndices = lod==0 ? (numPoints) : (numPoints * (tree.NumLodsPerCell-lod));
            if (lod!= 0 && divideMethod != CellDivideMethod.Linear) // Linear is already calculated above.
            {
                float exp = 0;
                switch (divideMethod)
                {
                    case CellDivideMethod.Quadratic2:
                        exp = 0.5f;
                        break;
                    case CellDivideMethod.Quadratic4:
                        exp = 0.25f;
                        break;
                    case CellDivideMethod.Quadratic8:
                        exp = 0.125f;
                        break;
                    case CellDivideMethod.Quadratic16:
                        exp = 0.125f/2;
                        break;
                }
                numIndices =  Mathf.RoundToInt( (numPoints) * Mathf.Pow( exp, lod ) );
            }
            return Mathf.Max( 1, numIndices );
        }

        internal void Verify()
        {
            if (cells == null)
            {
                if (!tree.IsLoadedFromCache)
                {
                    numPoints = vertices.Count;
                    Debug.Assert( vertices != null );
                }
                else
                {
                    Debug.Assert( leafIdx >= 0 );
                    Debug.Assert( vertices == null ); // Will be lazy loaded upon request (per voxel).
                }
                Debug.Assert( gfxData == null );
                Debug.Assert( !LeafReady );
                switch (tree.Loader.cellGfxDataType)
                {
                    case CellGfxDataType.Buffers:
                        gfxData = new LAZOctreeCellBuffers();
                        break;

                    case CellGfxDataType.Mesh:
                        gfxData = new LAZOctreeCellMesh( this );
                        break;
                }
            }
            else
            {
                Debug.Assert( vertices == null );
                Debug.Assert( gfxData == null );
                Debug.Assert( leafIdx == -1 );
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i].Verify();
                }
            }
        }

        internal void Traverse( Action<LAZOctreeCell, int> cb, int curTreeDepth )
        {
            cb( this, curTreeDepth );
            if (cells != null)
            {
                for (int i = 0;i < cells.Length;i++)
                {
                    cells[i].Traverse( cb, curTreeDepth + 1 );
                }
            }
        }
    }
}