﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace LAZNamespace
{
    [ExecuteInEditMode()]
    internal class LAZTraceDebugger : MonoBehaviour
    {
        public float radius = 0.2f;
        public float distance = 100;
        public int pointSkipStep = 100;
        public bool alsoQueryVertices = true;
        public float sphereVerticesRadius = 0.5f;
        public LAZLoader streamer;

        [Header("Debug")]
#if UNITY_EDITOR
        [ReadOnly] public long lastQueryTimeMs;
#endif

        TraceLineQuery query;
        TraceVerticesQuery verticesQuery;
        Vector3 foundPoint;
        bool hit;
        List<Vector3> spherePoints = new List<Vector3>();
        Stopwatch sw = new Stopwatch();

        private void Awake()
        {
            if (streamer == null )
                streamer = FindObjectOfType<LAZLoader>();
        }

        private void Update()
        {
            if (streamer == null)
            {
                if (Random.Range( 0, 30 )==0)
                    UnityEngine.Debug.LogWarning( "No streamer set, tracing will not work." );
                return;
            }
   
            if ( query == null && verticesQuery==null)
            {
                sw.Restart(); 
                query = streamer.StartLineTrace( transform.position, transform.forward, pointSkipStep, radius, distance );
            }
            else if ( query != null && query.done )
            {
#if UNITY_EDITOR
                sw.Stop();
                lastQueryTimeMs = sw.ElapsedMilliseconds;
#endif
                hit = query.hit; 
                if ( hit )
                {
                    foundPoint = query.vertex.position;

                    if (alsoQueryVertices)
                    {
                        if (verticesQuery == null)
                        {
                            verticesQuery = streamer.StartVertexQuery( foundPoint, sphereVerticesRadius );
                        }
                        else if (verticesQuery.done)
                        {
                            spherePoints.Clear();
                            for (int i = 0;i < verticesQuery.indices.Count;i++)
                            {
                                var rndr = verticesQuery.indices[i].Item1;
                                var tree = verticesQuery.indices[i].Item2;
                                var vIdx = verticesQuery.indices[i].Item3;
                                rndr.GetWorldTransform( out Matrix4x4 treeTransform );
                                tree.ApplyChangeToVertices( ( List<LAZVertex> vertices ) =>
                                {
                                    Vector3 worldPoint = treeTransform.MultiplyPoint3x4( vertices[vIdx].position );
                                    spherePoints.Add( worldPoint );
                                } );
                            }

                            // Restart the process
                            verticesQuery = null;
                            query = null;
                        }
                    }
                    else query = null;
                    
                }
                else query = null;

            }
        }

        private void OnDrawGizmos() 
        {
            if (!hit) 
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine( transform.position, transform.position + transform.forward*distance );
            }
            else
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine( transform.position, foundPoint );
                Gizmos.DrawSphere( foundPoint, 0.5f );
            }

            Gizmos.color = Color.cyan;
            for ( int i = 0; i < spherePoints.Count; i++ )
            {
                Gizmos.DrawSphere( spherePoints[i], 0.02f );
            }
        }
    }
}