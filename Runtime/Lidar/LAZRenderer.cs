using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.VFX;
using Wander;

namespace LAZNamespace
{
    public enum ViewType
    {
        VertexColor,
        Elevation,
        Classification,
        Intensity
    }

    public enum PlaceType
    {
        RelativeToOneSpecific,
        BringToCentre
    }

    [ExecuteAlways()]
    public class LAZRenderer : MonoBehaviour
    {
        [Tooltip("The loader used for loading LIDAR data.")]
        public LAZLoader loader;

        [Header("Stats")]
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public ulong NumPoints;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public ulong NumUsedPoints;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public bool LoadedFromCache;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float PercentageLoaded;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float SizeGb;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public float CompressRatio;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public int NumCells;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public int NumIntermediateCells;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        public int NumLeafs;
#if UNITY_EDITOR        
        [ReadOnly()]
#endif
        public int MaxTreeDepth;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        [Tooltip("Filename")]
        public string fileName;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        [Tooltip("Relative path from Assets")]
        public string relativePath;
#if UNITY_EDITOR
        [ReadOnly()]
#endif
        [Tooltip("Name of folder where this file is cached into asset bundles")]
        public string cacheFolder;

        [Header("Placing & Projection")]
        [Tooltip("Source CRS (Coordinate Reference System). In future, allow to read this from WKT from file.")]
        public Projections.Projection sourceProjection = Projections.Projection.NotSet;
        [Tooltip("Enter WKT to use that as CRS, otherwise sourceProjection above is used.")]
        public string sourceProjectionWKT;
        [Tooltip("Either place asset in centre of world or relative to a specific other asset.")]
        public PlaceType placeType = PlaceType.BringToCentre;

        [Header("VFX Visualisation")]
        [Tooltip("Max particles, put -1 to obtain max of data.")]
        public int maxNumParticles = -1;
        [Tooltip("Particle size when rendering using VFX. Otherwise does nothing.")]
        public float size = 0.02f;
        [Tooltip("Particle color scale when rendering using VFX. Otherwise does nothing.")]
        public float colorScale = 1.0f;
        [Tooltip("The Viewtype. Note each viewtype has its own associated material.")]
        public ViewType viewType = ViewType.VertexColor;
        [Tooltip( "The vfx used for when viewtype is color." )]
        public VisualEffectAsset vfxColor;
        [Tooltip("The vfx used for elevation.")]
        public VisualEffectAsset vfxElevation;
        [Tooltip( "The vfx used for when viewtype is intensity." )]
        public VisualEffectAsset vfxIntensity;
        [Tooltip( "The vfx used for when viewtype is classification." )]
        public VisualEffectAsset vfxClassification;

        [Header("Mesh Visualisation")]
        [Tooltip("The material used for rendering when viewtype is color.")]
        public Material colorMaterial;
        [Tooltip("The material used for rendering when viewtype is elevation.")]
        public Material elevationMaterial;
        [Tooltip("The material used for rendering when viewtype is intensity.")]
        public Material intensityMaterial;
        [Tooltip("The material used for rendering when viewtype is classification.")]
        public Material classificationMaterial;
        [Tooltip("LOD to switch when beyond this distance.")]
        [Range(0.1f, 1000)]
        public float LODDistance = 10;
        [Tooltip("LOD logaritmic fallof control.")]
        [Range(0.01f, 1)]
        public float LODLogaritmicFalloff = 0.45f;
        [Tooltip("LOD bias to add from calculated lod.")]
        [Range(-10, 10)]
        public int LODBias = 0;

        [Header("Elevation")]
        [Range(0, 1)]
        public float elevationMin = 0;
        [Range(0, 1)]
        public float elevationMax = 1;
        public Color elevationColorLow     = Color.blue;
        public Color elevationColorLowMid  = Color.green;
        public Color elevationColorMidHigh = Color.yellow;
        public Color elevationColorHighTop = Color.red;

        [Header("Intensity")]
        [Range(0.0f, 1.0f)]
        public float intensityMin = 0;
        [Range(0.0f, 1.0f)]
        public float intensityMax = 1;
        public Color intensityColorLow     = Color.blue;
        public Color intensityColorLowMid  = Color.green;
        public Color intensityColorMidHigh = Color.yellow;
        public Color intensityColorHighTop = Color.red;

        [Header("Classification")]
        public Texture2D classificationMap;
        public Color [] colors = new []
        {
            new Color(0.98f, 0.50f, 0.45f),
            new Color(1, 0.57f, 0.65f),
            new Color(0.76f, 0.7f, 0.5f),
            new Color(0.59f, 0.44f, 0.09f),
            new Color(0.96f, 0.64f, 0.38f),
            new Color(0.31f, 0.49f, 0.16f),
            new Color(0.06f, 0.32f, 0.73f),
            new Color(0.0f, 0.40f, 0.95f),
            new Color(0.18f, 0.66f, 0.63f),
            new Color(0.8f, 0.63f, 0.21f),
            new Color(1f, 0.14f, 0.0f),
            new Color(1f, 0.57f, 0.69f),
            new Color(1f, 0.85f, 0.0f),
            new Color(0.4f, 0.7f, 0.4f),
            new Color(0.4f, 1f, 0.6f),
            new Color(0.18f, 0.55f, 0.34f),
            new Color(0.0f, 1f, 0.8f),
            new Color(0.38f, 0.13f, 0.53f),
            new Color(0.2f, 0.08f, 0.08f),
        };

        [Header("Debug")]
        public bool showOctree = false;
        public bool showOctreeIfInvisible = false;
        [Range(-1, 20)]
        public int forceLOD = -1;

        public Color octreeColor = Color.blue;
        public Color [] leafColors = new [] { Color.red, Color.yellow, Color.blue, Color.green, Color.cyan, Color.magenta, Color.white, Color.black };

        // ------ Private -----------------------------------------------------------------------------------------------------------
        List<VisualEffect> vfxList;
        Matrix4x4 [] worldTransform;
        bool vfxListDirty;

        // Internal because need access from OnInspectorGUI()
        internal bool materialValuesDirty = true;
        internal LAZOctree lazFile;

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        LAZLoader GetLoader()
        {
            LAZLoader.GetSingle( ref loader );
            return loader;
        }

        private void Awake()
        {
            if (Application.isEditor)
            {
                GetLoader();

#if UNITY_EDITOR
                colorMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "db7cdc62c3ba4e14599e4034ab4d091d" ) );
                elevationMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "be827dbdbb6ea8b49a4888b15da4f481" ) );
                intensityMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "7fbd909c5325d9c48966563c665e2f2e" ) );
                classificationMaterial = AssetDatabase.LoadAssetAtPath<Material>( AssetDatabase.GUIDToAssetPath( "68bc4c0ada39be44aa98db8162ae8c43" ) );
                vfxElevation = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "c040bdee24cc9b54da9f9fb1f0a8900a" ) );
                vfxClassification = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "1c287fcc5b4d614468e11bb93e21734d" ) );
                vfxColor = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "0684482ab8d36f648924ace36ff37ed6" ) );
                vfxIntensity = AssetDatabase.LoadAssetAtPath<VisualEffectAsset>( AssetDatabase.GUIDToAssetPath( "f1912066fcc94894cad4f279e77de4d2" ) );
#endif
            }
        }

        // Nice hack here to ensure that when we create multiple gameobjects with an LazRender component to it,
        // we pospone Registering until the required paths are set. Do Ctrl-F on 'IsMassCreatingRenderers'
        internal static bool IsMassCreatingRenderers = false;

        private void OnEnable()
        {
            if (IsMassCreatingRenderers)
                return;

            worldTransform = new Matrix4x4[1];
            worldTransform[0] = transform.localToWorldMatrix;
            if (GetLoader() != null)
            {
                lazFile = loader.RegisterRenderer( this );
            }
        }

        private void OnDisable()
        {
            GetLoader()?.UnregisterRenderer( this );
        }

        private void OnDestroy()
        {
            GetLoader()?.UnregisterRenderer( this );
        }

        internal void MakeRelativeToThis()
        {
            if (lazFile != null)
            {
                GetLoader().SetRelativeTree( lazFile );
            }
            else
            {
                Debug.LogWarning( "Cannot make relative if has not registered a LAS file." );
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        int GetLod( int lodFromProvider, int maxLod )
        {
            int chosenLod;
            if (forceLOD >= 0)
            {
                chosenLod = Mathf.Clamp( forceLOD, 0, maxLod );
            }
            else
            {
                chosenLod = Mathf.Clamp( lodFromProvider+LODBias, 0, maxLod );
            }
            return chosenLod;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private void CreateColorMap()
        {
            if (classificationMap == null || classificationMap.width < 256)
            {
                classificationMap = new Texture2D( 256, 1 );
                Color [] colors = new Color[256];
                for (int i = 0;i < Mathf.Min( this.colors.Length, 256 );i++)
                {
                    colors[i] = this.colors[i];
                }
                classificationMap.SetPixels( colors );
                classificationMap.Apply( false, true );
            }
        }

        #region Mesh Rendering 

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void GetWorldTransform( out Matrix4x4 _worldTransform )
        {
            _worldTransform = worldTransform[0];
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        Matrix4x4 UpdateWorldTransform()
        {
            if (placeType == PlaceType.RelativeToOneSpecific)
            {
                LAZOctree referenceTree = loader.RelativeOctree;
                if (referenceTree == null)
                {
                    // Could be that the first started loading asset takes longer than 2nd or later started, so this might be a null ref for a short time.
                    // In that case, dont apply the offset yet.
                    worldTransform[0] = transform.localToWorldMatrix;
                }
                else
                {
                    // TODO optimize with caching
                    double dx = referenceTree.CentreWorldX - lazFile.CentreWorldX;
                    double dy = referenceTree.CentreWorldY - lazFile.CentreWorldY;
                    double dz = referenceTree.CentreWorldZ - lazFile.CentreWorldZ;
                    var translation = Matrix4x4.Translate( -new Vector3((float)dx, (float)dy, (float)dz) ); // Since all assets are in local space to preserve precision. Now, back compensate the offset of the specific.
                    worldTransform[0] = transform.localToWorldMatrix * translation;
                }
            }
            else
            {
                worldTransform[0] = transform.localToWorldMatrix;
            }
            return worldTransform[0];
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private Material GetAndUpdateRenderMaterial()
        {
            // TODO optimize this, parameters only need to be set once when not running PIE.
            var matToUse = colorMaterial;
            switch (viewType)
            {
                case ViewType.Elevation:
                {
                    matToUse = elevationMaterial;
                    if (materialValuesDirty)
                    {
                        matToUse?.SetVector( "_MinMaxPoint", new Vector2( lazFile.MinLocal.y, lazFile.MaxLocal.y ) );
                        matToUse?.SetColor( "_LowColor", elevationColorLow );
                        matToUse?.SetColor( "_LowMidColor", elevationColorLowMid );
                        matToUse?.SetColor( "_MidHighColor", elevationColorMidHigh );
                        matToUse?.SetColor( "_HighColor", elevationColorHighTop );
                        matToUse?.SetVector( "_MinMax", new Vector2( elevationMin, elevationMax ) );
                    }
                }
                break;

                case ViewType.Intensity:
                {
                    matToUse = intensityMaterial;
                    if (materialValuesDirty)
                    {
                        matToUse?.SetColor( "_LowColor", intensityColorLow );
                        matToUse?.SetColor( "_LowMidColor", intensityColorLowMid );
                        matToUse?.SetColor( "_MidHighColor", intensityColorMidHigh );
                        matToUse?.SetColor( "_HighColor", intensityColorHighTop );
                        matToUse?.SetVector( "_MinMax", new Vector2( intensityMin, intensityMax ) );
                    }
                }
                break;

                case ViewType.Classification:
                {
                    CreateColorMap();
                    matToUse = classificationMaterial;
                    if (materialValuesDirty)
                    {
                        matToUse?.SetTexture( "_ColorMap", classificationMap );
                    }
                }
                break;
            }

            materialValuesDirty = false;
            return matToUse;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void RenderMesh( LAZOctreeCellMesh meshData, int lod )
        {
            Debug.Assert( loader.cellGfxDataType == CellGfxDataType.Mesh );
            Material matToUse = GetAndUpdateRenderMaterial();

            if (matToUse == null)
            {
                Debug.LogWarning( "Material is null. Cannot render without material." );
                return;
            }

            var mesh = meshData.Mesh;
            if (mesh != null)
            {
                Debug.Assert( lod >= 0 && lod < mesh.subMeshCount );
                lod = GetLod( lod, mesh.subMeshCount-1 );
                Graphics.DrawMesh( mesh, worldTransform[0], matToUse, 0, null, lod, null, true );
            }
        }

        #endregion

        #region Visual Effect rendering 

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private VisualEffectAsset GetVisualEffect()
        {
            VisualEffectAsset asset = vfxElevation;
            switch (viewType)
            {
                case ViewType.Intensity:
                    asset = vfxIntensity;
                    break;
                case ViewType.VertexColor:
                    asset = vfxColor;
                    break;
                case ViewType.Classification:
                    asset = vfxClassification;
                    break;
            }
            return asset;
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        void UpdateVisualEffectParams( VisualEffect vfx, LAZOctreeCellBuffers cellBuffers )
        {
            Debug.Assert( vfx != null );

            uint numParticlesToShow = maxNumParticles==-1 ? (uint)lazFile.NumLoadedPoints : (uint)maxNumParticles;
            vfx.SetUInt( "count", numParticlesToShow );
            vfx.SetFloat( "size", size );
            vfx.SetGraphicsBuffer( "positionBuffer", cellBuffers.PositionBuffer );
            vfx.SetMatrix4x4( "transform", worldTransform[0] );
            vfx.SetFloat( "colorScale", colorScale );

            switch (viewType)
            {
                case ViewType.Elevation:
                {
                    vfx.SetVector2( "minMaxPoint", new Vector2( lazFile.MinLocal.y, lazFile.MaxLocal.y ) );
                    vfx.SetVector2( "minMax", new Vector2( elevationMin, elevationMax ) );
                    vfx.SetVector4( "color1", elevationColorLow );
                    vfx.SetVector4( "color2", elevationColorLowMid );
                    vfx.SetVector4( "color3", elevationColorMidHigh );
                    vfx.SetVector4( "color4", elevationColorHighTop );
                }
                break;

                case ViewType.Intensity:
                    vfx.SetGraphicsBuffer( "colorBuffer", cellBuffers.ColorBuffer );
                    vfx.SetVector2( "minMax", new Vector2( intensityMin, intensityMax ) );
                    vfx.SetVector4( "color1", intensityColorLow );
                    vfx.SetVector4( "color2", intensityColorLowMid );
                    vfx.SetVector4( "color3", intensityColorMidHigh );
                    vfx.SetVector4( "color4", intensityColorHighTop );
                    break;

                case ViewType.VertexColor:
                    vfx.SetGraphicsBuffer( "colorBuffer", cellBuffers.ColorBuffer );
                    break;

                case ViewType.Classification:
                    CreateColorMap();
                    vfx.SetGraphicsBuffer( "colorBuffer", cellBuffers.ColorBuffer );
                    vfx.SetTexture( "classificationMap", classificationMap );
                    break;
            }
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void RenderVisualEffect( LAZOctreeCellBuffers bufferData, int lod )
        {
            Debug.Assert( loader.cellGfxDataType == CellGfxDataType.Buffers );

            var vfxAsset = GetVisualEffect();
            if (vfxAsset == null)
            {
                Debug.LogWarning( "Visual effect is null. Cannot render buffers without." );
                return;
            }

            //            lock (renderList)
            //            {
            //                int count = renderList.Count;

            //                // Lazy initialise the Vfx game objects.
            //                if (vfxList == null || vfxList.Count != count)
            //                {
            //                    // Delete former
            //                    var vfxs = gameObject.GetComponentsInChildren<VisualEffect>();
            //                    foreach (var fvx in vfxs)
            //                    {
            //                        fvx.gameObject.Destroy();
            //                    }
            //                    // Set up new
            //                    vfxList = new List<VisualEffect>( count );
            //                    for (int i = 0;i < count;i++)
            //                    {
            //                        GameObject goVfx = new GameObject("Vfx octree cell");
            //                        var vfx = goVfx.AddComponent<VisualEffect>();
            //                        vfx.visualEffectAsset = vfxAsset;
            //                        vfxList.Add( vfx );
            //                        goVfx.transform.SetParent( gameObject.transform );
            //                    }
            //                    // mark dirty 
            //                    vfxListDirty = true;
            //                }

            //#if UNITY_EDITOR
            //                // Only in editor, we can adjust this.
            //                for (int i = 0;i < count;i++)
            //                {
            //                    vfxList[i].visualEffectAsset = vfxAsset;
            //                }
            //                vfxListDirty = true;
            //#endif
            //                if (vfxListDirty)
            //                {
            //                    for (int i = 0;i < count;i++)
            //                    { 
            //                        LAZOctreeCellBuffers cellBuffers = renderList[i].Item1.gfxData as LAZOctreeCellBuffers;
            //                        UpdateVisualEffectParams( vfxList[i], cellBuffers );
            //                    }
            //                }
            //            }
        }

        #endregion

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void RenderCell( LAZOctreeCell cell, int lod )
        {
            UpdateWorldTransform();

            if (loader.cellGfxDataType == CellGfxDataType.Mesh)
            {
                LAZOctreeCellMesh meshData = cell.gfxData as LAZOctreeCellMesh;
                RenderMesh( meshData, lod );
            }
            else
            {
                LAZOctreeCellBuffers bufferData = cell.gfxData as LAZOctreeCellBuffers;
                RenderVisualEffect( bufferData, lod );
            }

#if UNITY_EDITOR
            if ( showOctree ) // Debug
            {
                lod            = GetLod( lod, leafColors.Length-1 );
                Vector3 centre = worldTransform[0].MultiplyPoint( cell.Centre );
                MiscUtils.DrawBox( centre, Quaternion.identity, Vector3.Scale( cell.HalfSize*2, transform.localScale ), leafColors[lod] );
            }
#endif
        }

        private void OnDrawGizmos()
        {
            if (lazFile == null || !lazFile.IsReady)
                return;

            // Show if leaf is visible or not.
            if (showOctreeIfInvisible && GetLoader().cullingType != CullingType.None)
            {
                Gizmos.color   = Color.black;
                lazFile.Traverse( ( cell, td ) =>
                {
                    if (cell.IsLeaf && !cell.IsVisible)
                    {
                        Vector3 centre = worldTransform[0].MultiplyPoint( cell.Centre );
                        Gizmos.DrawWireCube( centre, Vector3.Scale( cell.HalfSize*2, transform.localScale ) );
                    }
                } );
            }
        }
    }

    #region Unity Inspector 

#if UNITY_EDITOR
    [CustomEditor( typeof( LAZRenderer ) ), CanEditMultipleObjects]
    [InitializeOnLoad]
    internal class LAZRendererEditor : Editor
    {
        static LAZRendererEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            LAZRenderer lazRenderer = (LAZRenderer)target;
            lazRenderer.materialValuesDirty = true;

            if ( !lazRenderer.enabled)
            {
                EditorGUILayout.HelpBox( "Enable to start rendering process", MessageType.Warning, true );
            }

            ulong bufferParticleCapacity = 10000000;
            if (lazRenderer.loader != null && lazRenderer.lazFile != null &&
                 lazRenderer.loader.cellGfxDataType == CellGfxDataType.Buffers &&
                 (lazRenderer.lazFile.NumLoadedPoints > bufferParticleCapacity  &&
                 (lazRenderer.maxNumParticles == -1 || lazRenderer.maxNumParticles > (long)bufferParticleCapacity)))
            {
                EditorGUILayout.HelpBox( "Max capacity of buffer point count is 10.000.000, not all points are rendererd.", MessageType.Warning, true );
            }

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Select file" ))
                {
                    string filePath = EditorUtility.OpenFilePanel("Select path", "", "" );
                    if (!string.IsNullOrEmpty( filePath ))
                    {
                        lazRenderer.fileName     = Path.GetFileName( filePath );
                        lazRenderer.relativePath = filePath.ToAssetsPath( false ).Replace( lazRenderer.fileName, "" ).TrimEnd( '/' );
                        lazRenderer.cacheFolder  = Path.GetFileNameWithoutExtension( filePath );
                        if (lazRenderer.relativePath.Contains( filePath ))
                        {
                            Debug.LogError( "A file is chosen not relative to the current project folder. This is not valid." );
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty( lazRenderer.fileName ) &&
                                !string.IsNullOrEmpty( lazRenderer.relativePath ) &&
                                !string.IsNullOrEmpty( lazRenderer.cacheFolder ))
                            {
                                Repaint();
                                if (lazRenderer.enabled)
                                {
                                    lazRenderer.enabled=false; // Unregisters old
                                    lazRenderer.enabled=true;  // registers new
                                }
                            }
                            else
                            {
                                Debug.LogWarning( "Path to file is not valid, please check inspector what it displays at: FileName, RelativePath and CacheFolder!" );
                            }
                        }
                    }
                }

                if (GUILayout.Button( "Select folder" ))
                {
                    string folderPath = EditorUtility.OpenFolderPanel("Select folder", lazRenderer.cacheFolder, "" ).NormalizePath();
                    if (!string.IsNullOrEmpty( folderPath ))
                    {
                        // Assume ascii files..
                        List<LAZRenderer> newLazRenderers = new List<LAZRenderer>();
                        try
                        {
                            LAZRenderer.IsMassCreatingRenderers=true; // Prevents calling OnEnable when Adding component LAZRenderer.
                            var files = Directory.GetFiles( folderPath, "*.txt" );
                            for (int i = 0;i<files.Length;i++)
                            {
                                var filename    = Path.GetFileName(files[i]);
                                GameObject go   = new GameObject("Points " +  filename);
                                var lr          = go.AddComponent<LAZRenderer>();
                                lr.fileName     = filename;
                                lr.relativePath = folderPath.ToAssetsPath( false );
                                lr.cacheFolder  = Path.GetFileNameWithoutExtension( files[i] );
                                newLazRenderers.Add( lr );
                            }
                        }
                        finally
                        {
                            LAZRenderer.IsMassCreatingRenderers = false;
                            newLazRenderers.ForEach( lr =>
                            {
                                lr.enabled=false;
                                lr.enabled=true; //calls OnEnable
                            } );
                        }
                    }
                }
                if (GUILayout.Button( "Reposition all relative to this, this one becomes centre" ))
                {
                    lazRenderer.MakeRelativeToThis();
                }
            }
            GUILayout.EndHorizontal();
            DrawDefaultInspector();

            // Called from LAZLoader to update stats as it is loaded. The stats belong the the LAZOctree, but for convinience, this is coppied
            // to the LAZRenderer.
            UpdateInspectorStats( lazRenderer );
        }

        void UpdateInspectorStats( LAZRenderer lazRenderer )
        {
            if (lazRenderer == null)
                return;

            if (lazRenderer.lazFile != null)
            {
                lazRenderer.NumPoints = lazRenderer.lazFile.NumPoints;
                lazRenderer.NumUsedPoints = lazRenderer.lazFile.NumLoadedPoints;
                lazRenderer.PercentageLoaded = lazRenderer.lazFile.PercentageDone;
                lazRenderer.SizeGb = lazRenderer.lazFile.sizeGB;
                lazRenderer.CompressRatio = lazRenderer.lazFile.compressRatio;
                lazRenderer.NumCells = lazRenderer.lazFile.numCells;
                lazRenderer.NumIntermediateCells = lazRenderer.lazFile.numIntermediateCells;
                lazRenderer.NumLeafs = lazRenderer.lazFile.numLeafs;
                lazRenderer.MaxTreeDepth = lazRenderer.lazFile.maxTreeDepth;
                lazRenderer.LoadedFromCache = lazRenderer.lazFile.IsLoadedFromCache;
            }
        }

    }
#endif

    #endregion
}