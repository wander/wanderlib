﻿using System.Collections.Generic;
using System.IO;
using Unity.Netcode;
using UnityEngine;

namespace Wander
{
    public class NetworkHelper :MonoBehaviour
    {
        [Tooltip("Auto starts ")]
        public bool autoStartInEditor = false;

        private NetworkManager netManager;

        [Tooltip("This is only valid in PIE and with ParrelSync plugin.")]
        static bool? _isClone;
        public static bool IsClone
        {
            get
            {
                if (_isClone == null)
                {
                    var path = Application.dataPath + "/../.clone";
                    _isClone = File.Exists( path ); // Note this a slow function, cache result.
                }
                return _isClone.Value;
            }
        }

        void Start()
        {
            netManager = GetComponentInParent<NetworkManager>();

            if (Application.isEditor)
            {
                if (autoStartInEditor)
                {
                    if (!IsClone)
                    {
                        netManager.StartHost();
                    }
                }
                return;
            }

            var args = GetCommandlineArgs();

            // check for network type
            if ( args.TryGetValue( "-mlapi", out string mlapiValue ) )
            {
                switch ( mlapiValue )
                {
                    case "server":
                        netManager.StartServer();
                        break;
                    case "host":
                        netManager.StartHost();
                        break;
                    case "client":
                        netManager.StartClient();
                        break;
                }
            }
        }

        public static Dictionary<string, string> GetCommandlineArgs()
        {
            Dictionary<string, string> argDictionary = new Dictionary<string, string>();

            var args = System.Environment.GetCommandLineArgs();

            for ( int i = 0;i < args.Length;++i )
            {
                var arg = args[i].ToLower();
                if ( arg.StartsWith( "-" ) )
                {
                    var value = i < args.Length - 1 ? args[i + 1].ToLower() : null;
                    value = (value?.StartsWith( "-" ) ?? false) ? null : value;
                    argDictionary.Add( arg, value );
                }
            }
            return argDictionary;
        }
    }
}