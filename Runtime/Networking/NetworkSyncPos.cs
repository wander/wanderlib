using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.Assertions;

namespace Wander
{
    // Alternative to Unitys NetworkTransform which seems to add delay to the interpolation eventhough it should be faster.. according to their comments.
    public class NetworkSyncPos : NetworkBehaviour
    {
        public float lerpSmooth = 15;
        public bool doPosition;
        public bool doYaw;
        public bool doPitch;
        public bool doRoll;

        [Tooltip("0 or negative prevents a teleport.")]
        public float teleportAtDistance = 0;
        [Tooltip("0 or negative prevents a threshold check.")]
        public float posThreshold = 0.01f;
        [Tooltip("0 or negative prevents a threshold check.")]
        public float angThreshold = 0.1f;

        NetworkVariable<Vector3> networkPosition    = new NetworkVariable<Vector3>(default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        NetworkVariable<float> networkYaw           = new NetworkVariable<float>(default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        NetworkVariable<float> networkPitch         = new NetworkVariable<float>(default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        NetworkVariable<float> networkRoll          = new NetworkVariable<float>(default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);

        void Update()
        {
            if ( IsOwner )
            {
                if (doPosition && (posThreshold<=0 || Vector3.Distance( transform.position, networkPosition.Value ) > posThreshold))
                {
                    networkPosition.Value = transform.position;
                }

                if (doPitch || doRoll || doYaw)
                {
                    var euler = transform.rotation.eulerAngles;
                    bool enoughAngleDiff = true;
                    if (angThreshold > 0)
                    {
                        float ang = Mathf.Abs( Quaternion.Angle( transform.rotation, Quaternion.Euler( networkPitch.Value, networkYaw.Value, networkRoll.Value ) ) );
                        enoughAngleDiff = (ang >= angThreshold);
                    }
                    if (enoughAngleDiff)
                    {
                        if (doYaw) networkYaw.Value     = euler.y;
                        if (doPitch) networkPitch.Value = euler.x;
                        if (doRoll) networkRoll.Value   = euler.z;
                    }
                }
            }
            else // If not owner, interpolate to the provided values.
            {
                var smooth = MathUtil.Smooth(lerpSmooth, Time.deltaTime);
                if (doPosition)
                {
                    if (teleportAtDistance > 0 &&
                        transform.position.DistSq( networkPosition.Value ) >= teleportAtDistance*teleportAtDistance )
                    {
                        transform.position = networkPosition.Value;
                    }
                    else
                    {
                        transform.position = Vector3.LerpUnclamped( transform.position, networkPosition.Value, smooth );
                    }
                }
                if (doPitch || doRoll || doYaw)
                {
                    transform.rotation = Quaternion.Slerp( transform.rotation, Quaternion.Euler(
                        doPitch ? networkPitch.Value : 0,
                        doYaw ? networkYaw.Value : 0,
                        doRoll ? networkRoll.Value : 0 ),
                        smooth );
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            //   Gizmos.DrawWireCube( bounds.center, bounds.size );
        }
    }
}