﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unity.Netcode.Transports.UTP;
using UnityEngine;

namespace Wander
{
    public class ServerEntry
    {
        public string name;
        public string ip;
        public ushort port;
        public long aliveTimestamp;
    }

    public class NetworkDiscovery :MonoBehaviour
    {
        public bool autoStartInEditor = false;
        public int discoveryPort = 8837;
        public float deadServerTimeout = 10;
        public string serverTag = "LIDAR";
        public string serverName = "MyServerName";

        UnityTransport transport;
        List<ServerEntry> serverEntries = new List<ServerEntry>();
        UdpClient udpClient;
        Task discoveryTask;

        private void Awake()
        {
            transport = GetComponent<UnityTransport>();
        }

        private void Start()
        {
            StartCoroutine( CleanupLists() );

            if (autoStartInEditor && Application.isEditor)
            {
                if (NetworkHelper.IsClone)
                {
                    StartReceiving();
                }
                else
                {
                    StartBroadcasting();
                }
            }
        }

        public List<ServerEntry> GetServerEntries()
        {
            List<ServerEntry> newEntries = new List<ServerEntry>();
            lock(serverEntries)
            {
                for ( int i = 0; i < serverEntries.Count; i++ )
                {
                    newEntries.Add( serverEntries[i] );
                }
            }
            return newEntries;
        }

        void AddServerEntry(string _name, string _ip, ushort _port)
        {
            lock (serverEntries)
            {
                var idx = serverEntries.FindIndex( se => se.ip==_ip&&se.port==_port );
                var secondsNow = DateTimeOffset.Now.ToUnixTimeSeconds();
                if (idx < 0)
                { // is new
                    serverEntries.Add( new()
                    {
                        name = _name,
                        ip = _ip,
                        port = _port,
                        aliveTimestamp = secondsNow
                    });
                }
                else
                {
                    // Only update timestamp
                    serverEntries[idx].aliveTimestamp = secondsNow; 
                }
            }
        }

        public void StartReceiving()
        {
            Debug.Assert( udpClient == null && discoveryTask == null );
            Debug.Log( "Started Receiving.." );
            udpClient = new UdpClient();
            udpClient.ExclusiveAddressUse = false;
            udpClient.Client.Bind( new IPEndPoint( IPAddress.Any, discoveryPort ) );
            var task = Task.Run(() =>
            {    
                while (true)
                {
                    var from = new IPEndPoint(0, 0);
                    var recvBuffer = udpClient.Receive(ref from);
                    var msg = Encoding.UTF8.GetString(recvBuffer);
                    var args = msg.Split("_", StringSplitOptions.RemoveEmptyEntries );
                    if (args.Length==3 && args[0] == serverTag)
                    {
                        var name = args[1]; // server name 
                        var ip   = from.Address.ToString().Split(":", StringSplitOptions.RemoveEmptyEntries)[0]; // address is identical to network discovery address
                        var port = ushort.Parse( args[2] ); // port is not identical, is sent as payload
                        AddServerEntry(name, ip, port );
                    }
                }
            });
        }

        public void StartBroadcasting()
        { 
            Debug.Assert( udpClient == null && discoveryTask == null );
            Debug.Log( "Started Broadcasting.." );
            udpClient = new UdpClient();
            var data = Encoding.UTF8.GetBytes(serverTag + "_" + serverName + "_" + transport.ConnectionData.Port);
            var broadcastAddr = new IPEndPoint( IPAddress.Broadcast, discoveryPort );
            discoveryTask = Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(1500);
                    udpClient.Send( data, data.Length, broadcastAddr );
                }
            });
        }

        IEnumerator CleanupLists()
        {
            while (true)
            {
                var secondsNow = DateTimeOffset.Now.ToUnixTimeSeconds();
                lock (serverEntries)
                {
                    for ( int i = serverEntries.Count-1; i>=0; i--)
                    {
                        var lastUpdateTime = secondsNow - serverEntries[i].aliveTimestamp;

                        // For debugging purposes
                        Debug.Log( "BroadcastServer: " +  serverEntries[i].name + " ip: " + serverEntries[i].ip + " port: " + serverEntries[i].port +  " time delta : " + lastUpdateTime );

                        // Cleanup servers which dont respond aymore
                        if (lastUpdateTime > deadServerTimeout)
                        {
                            serverEntries.RemoveAt(i);
                        }
                    }
                }
                yield return new WaitForSecondsRealtime( 2 );
            }
        }

        public void StopDiscovery()
        {
            if ( udpClient != null )
            {
                try
                {
                    udpClient.Close();
                }
                catch (Exception)
                {
                    // Silently ignore these, other thread is waiting on data possibly, so this wil raise an unexpected close exception or similar.
                }
                udpClient = null;
            }
            if (discoveryTask!=null)
            {
                discoveryTask.Wait();
                discoveryTask=null;
            }
        }
    }
}