using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.Assertions;

namespace Wander
{
    public class FPSMove :NetworkBehaviour
    {
        [Header("Movement")]
        public float moveAccel  = 0.2f;
        public float brakeSpeed = 0.5f;
        public float boostAccel = 0.2f;
        public float gravity = -0.5f;

        [Header("Looking")]
        public float lookSpeed  = 0.2f;
        public Transform armsRotor;

        [Header("Lerping")]
        public float lerpSmooth = 15;

        CharacterController controller;
        NetworkVariable<Vector3> networkPosition = new NetworkVariable<Vector3>();
        NetworkVariable<float> networkYaw = new NetworkVariable<float>();
        NetworkVariable<float> networkPitch = new NetworkVariable<float>();

        float yaw;
        float pitch;
        Vector3 velocity;
        Vector3 targetPosition;

        private void Awake()
        {
            // Network transform conflicts with our own sync method.
            var nt = GetComponent<NetworkTransform>();
            Assert.IsNull( nt );
            controller = GetComponent<CharacterController>();
        }

        void Start()
        {
            targetPosition = transform.position;
            yaw = transform.rotation.eulerAngles.y;
            pitch = transform.rotation.eulerAngles.x;
            if ( IsServer )
            {
                networkPosition.Value = targetPosition;
                networkYaw.Value = yaw;
                networkPitch.Value = pitch;
            }
            Debug.Log( "Started script" );
        }

        public override void OnNetworkSpawn()
        { 
            Debug.Log( "Spawned network" );
            if ( !IsOwner ) GetComponent<CharacterController>().Destroy();
            //if (IsOwner)
            //{
            //    FindObjectOfType<ThirdPersonCam>().target = transform;
            //}
        }

        public override void OnNetworkDespawn()
        {
            Debug.Log( "Network despawn" );
        }

        public override void OnGainedOwnership()
        {
            Debug.Log( "Got ownership" );
        }

        public override void OnLostOwnership()
        {
            Debug.Log( "Lost ownership" );
        }

        void Update()
        {
            var smooth = (1- Mathf.Pow( 0.5f, Time.deltaTime*lerpSmooth ));// TODO is wrong. fix with MathUtils.Friction or Smooth.
            if ( !IsOwner )
            {
                transform.position = Vector3.LerpUnclamped( transform.position, networkPosition.Value, smooth );
                transform.rotation = Quaternion.Slerp( transform.rotation, Quaternion.Euler( 0, networkYaw.Value, 0 ), smooth );
                if ( armsRotor != null )
                {
                    armsRotor.localRotation = Quaternion.Slerp( armsRotor.localRotation, Quaternion.Euler( networkPitch.Value, 0, 0 ), smooth );
                }
            }
            else
            {
                transform.position = Vector3.LerpUnclamped( transform.position, targetPosition, smooth );
                transform.rotation = Quaternion.Slerp( transform.rotation, Quaternion.Euler( 0, yaw, 0 ), smooth );
                if ( armsRotor != null )
                {
                    armsRotor.localRotation = Quaternion.Slerp( armsRotor.localRotation, Quaternion.Euler( pitch, 0, 0 ), smooth );
                }
            }
        }

        private void FixedUpdate()
        {
            if ( !IsOwner )
                return;

            float ver = 0;
            float hor = 0;
            float boost = 0;
            ver += (Input.GetKey( KeyCode.W ) || Input.GetKey( KeyCode.UpArrow )) ? 1 : 0;
            ver += (Input.GetKey( KeyCode.S ) || Input.GetKey( KeyCode.DownArrow )) ? -1 : 0;
            hor += (Input.GetKey( KeyCode.A ) || Input.GetKey( KeyCode.LeftArrow )) ? -1 : 0;
            hor += (Input.GetKey( KeyCode.D ) || Input.GetKey( KeyCode.RightArrow )) ? 1 : 0;
            if ( Input.GetKey( KeyCode.Space ) ) boost = boostAccel;
            Vector3 accel = new Vector3(hor, 0, ver) * (moveAccel+boost);
            if ( !controller.isGrounded ) accel.y += gravity;
            else velocity.y = 0;
            velocity += accel;
            velocity *= brakeSpeed;

            float x = Input.GetAxisRaw( "Mouse X" );
            float y = Input.GetAxisRaw( "Mouse Y" );
            yaw += x * lookSpeed;
            pitch += -y * lookSpeed;
            if ( yaw < 0 ) yaw += 360;
            if ( yaw > 360 ) yaw -= 360;
            if ( pitch < -50 ) pitch = -50;
            if ( pitch > 50 ) pitch = 50;

            Vector3 velWorld = Quaternion.Euler(0, yaw, 0 ) * velocity;
            Vector3 oldPos = transform.position;
            controller.Move( velWorld );
            targetPosition += transform.position-oldPos;
            transform.position = oldPos;

            if ( IsServer )
            {
                networkPosition.Value = targetPosition;
                networkYaw.Value = yaw;
                networkPitch.Value = pitch;
            }
            else
            {
                PositionServerRpc( targetPosition, yaw, pitch );
            }
        }

        // Send adjusted postion/orientation from client to server.
        // Server then adjusts the transform which is auto synced with NetworkTransform to other clients
        [ServerRpc( Delivery = RpcDelivery.Unreliable )]
        void PositionServerRpc( Vector3 pos, float yawIn, float pitchIn )
        {
            targetPosition = pos;
            yaw = yawIn;
            pitch = pitchIn;
            networkPosition.Value = targetPosition;
            networkYaw.Value = yaw;
            networkPitch.Value = pitch;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            //   Gizmos.DrawWireCube( bounds.center, bounds.size );
        }
    }
}