using UnityEngine;
using System.Collections;
using System.Diagnostics;
#if UNITY_EDITOR
using UnityEditor;
#endif


#if CO_ROUTINES_WORK
using Unity.EditorCoroutines.Editor;
#endif

namespace Wander
{
    public class PLYToGameObject : MonoBehaviour
    {
        public string plyFile = "";
        public Material material;
        public bool addNormalsIfMissing = true;
        public bool showWarnings = true;
        public bool doUnsafe = true;

        public void Generate()
        {
            //StartCoroutine( LoadPlyAsync() );
#if UNITY_EDITOR
            /* TODO adding Editor Coroutines should solve this dependency but it does not!! */
         //  EditorCoroutineUtility.StartCoroutine( LoadPlyAsync2(), this );
            //   EditorCoroutineUtility.StartCoroutine( LoadPlyAsync(), this );
#endif

        }

        IEnumerator LoadPlyAsync()
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            var c = new WaitForArrayFromPlyAsync( plyFile, addNormalsIfMissing, false, true );
            yield return c;
            if ( c.Succesful )
            {
                Mesh m = c.Arrays.ToMesh();
                GameObject go = new GameObject("PlyFile");
                go.AddComponent<MeshRenderer>().sharedMaterial = material;
                go.AddComponent<MeshFilter>().sharedMesh = m;
                UnityEngine.Debug.Log( "LoadPlayAsync call time: " + st.Elapsed.TotalSeconds + " seconds" );
            }
        }

        IEnumerator LoadPlyAsync2()
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            var c = new WaitForNatArrayFromPlyAsync( plyFile, false, true );
            yield return c;
            if ( c.Succesful )
            {
                Mesh m = c.ToMesh( true );
                GameObject go = new GameObject("PlyFile");
                go.AddComponent<MeshRenderer>().sharedMaterial = material;
                go.AddComponent<MeshFilter>().sharedMesh = m;
                st.Stop();
                UnityEngine.Debug.Log( "LoadPlayAsync2 call time: " + st.Elapsed.TotalSeconds + " seconds" );
            }
            c.Dispose();
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( PLYToGameObject ) )]
    [InitializeOnLoad]
    public class PLYToGameObjectEditor : Editor
    {
        static PLYToGameObjectEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            PLYToGameObject settings = (PLYToGameObject)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select ply file" ) )
                {
                    settings.plyFile = EditorUtility.OpenFilePanel( settings.plyFile, "", "ply" );
                }
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    if ( !string.IsNullOrWhiteSpace( settings.plyFile ) )
                    {
                        settings.Generate();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
    }
#endif

}