using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Wander
{
    public class PCacheToPLY : MonoBehaviour
    {
        public string pcacheFile = "";
        public bool asBinary = false;

        public void Generate()
        {
            PLYUtils.CreatePlyFromPointCache( pcacheFile, asBinary );
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( PCacheToPLY ) )]
    [InitializeOnLoad]
    public class PCacheToPLYEditor : Editor
    {
        static PCacheToPLYEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            PCacheToPLY settings = (PCacheToPLY)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select pcache file" ) )
                {
                    settings.pcacheFile = EditorUtility.OpenFilePanel( settings.pcacheFile, "", "pcache" );
                }
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    if ( !string.IsNullOrWhiteSpace( settings.pcacheFile ) )
                    {
                        settings.Generate();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
    }
#endif

}