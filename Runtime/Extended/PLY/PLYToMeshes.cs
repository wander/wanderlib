using UnityEngine;
using System.IO;
using System.Collections;
#if UNITY_EDITOR
//using Unity.EditorCoroutines.Editor;
using UnityEditor;
#endif

namespace Wander
{
    public class PLYToMeshes : MonoBehaviour    
    {
        public string inputFolder;
        public string outputFolder;
        public bool generateNormals = true;
        public bool showWarnings = true;
        public bool doUnsafe = true;

        internal float progress;

        bool IsValidFile( string f )
        {
            if ( Path.GetExtension(f) != ".ply" )
                return false;

            var filename   = Path.GetFileNameWithoutExtension( f ) + ".mesh";
            var outputname = Path.Combine( outputFolder, filename ).Replace("\\","/");

            if ( File.Exists( outputname ) )
            {
                return false;
            }
            return true;
        }

        public IEnumerator Generate()
        {
#if UNITY_EDITOR

            progress = 0;
            int numFilesToProcess = 0;
            
            var files = Directory.GetFiles(inputFolder);
            
            // Count number of files to show live progress
            foreach ( var f in files )
            {
                if ( !IsValidFile( f ) )
                    continue;
                numFilesToProcess++;
            }

            // Process files
            int processed = 0;
            foreach ( var f in files )
            {
                if ( !IsValidFile( f ) )
                    continue;

                var filenameIn   = Path.GetFileNameWithoutExtension( f ) + ".ply";
                var filenameOut  = Path.GetFileNameWithoutExtension( f ) + ".mesh";
                var intputName   = Path.Combine( inputFolder, filenameIn ).Replace("\\","/");
                var outputName   = Path.Combine( outputFolder, filenameOut ).Replace("\\","/");

                Mesh mesh = PLYUtils.CreateMeshFromPly( intputName, generateNormals, showWarnings, doUnsafe );
                AssetDatabase.CreateAsset( mesh, outputName.ToAssetsPath() );

                processed++;
                progress = (float)processed/numFilesToProcess;

                yield return null;
            }

#endif

            yield break;
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( PLYToMeshes ) )]
    [InitializeOnLoad]
    public class PLYToMeshesEditor : Editor
    {
        static PLYToMeshesEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            PLYToMeshes settings = (PLYToMeshes)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select folder" ) )
                {
                    string s = EditorUtility.OpenFolderPanel( settings.inputFolder, "", "" );
                    if ( !string.IsNullOrEmpty( s ) )
                        settings.inputFolder = s;
                }
                if ( GUILayout.Button( "Select folder" ) )
                {
                    string s =  EditorUtility.OpenFolderPanel( settings.outputFolder, "", "" );
                    if ( !string.IsNullOrEmpty( s ) )
                        settings.outputFolder = s;
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    // TODO
 //                   EditorCoroutineUtility.StartCoroutine( settings.Generate(), this );
                }
            }
            GUILayout.EndHorizontal();

            Rect rc = EditorGUILayout.BeginVertical();
            EditorGUI.ProgressBar( rc, settings.progress, "Num Meshes" );
            EditorGUILayout.EndVertical();
   
        }
    }
#endif

}