﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Wander
{
    class PLYParser
    {
        internal enum ReadType
        {
            Char,
            Uchar,
            Short,
            UShort,
            Int,
            Uint,
            Float,
            Double
        }

        internal struct ReadContext
        {
            internal byte [] data;
            internal int readPosition;
            internal bool isBinary;
            internal int numVertices;
            internal int numFaces;
            internal int numVertexAttributes;
            internal Vector3Int positionOffset;
            internal Vector3Int normalOffset;
            internal Vector3Int colorOffset;
            internal int alphaOffset;
            internal List<ReadType> vertexDataTypes;
            internal List<double> vertexData;
            internal ReadType indexCountType;
            internal ReadType indicesType;
            internal List<int> indices;

            internal bool HasPositions()
            {
                return positionOffset.x != -1 && positionOffset.y != -1 && positionOffset.z != -1;
            }

            internal bool HasNormals()
            {
                return normalOffset.x != -1 && normalOffset.y != -1 && normalOffset.z != -1;
            }

            internal bool HasColors()
            {
                return colorOffset.x != -1 && colorOffset.y != -1 && colorOffset.z != -1;
            }

            internal bool HasAlpha()
            {
                return alphaOffset != -1;
            }
        }

        private static bool HasDoubleFaces( ref ReadContext rc, bool hasDoubleFaces )
        {
            List<int> t0 = new List<int>();
            List<int> t1 = new List<int>();
            for ( int i = 0; i < 6; i++ )
            {
                t0.Add( rc.indices[i] );
                t1.Add( rc.indices[i+6] );
            }
            t0.Sort();
            t1.Sort();
            return t0.SequenceEqual( t1 );
        }

        internal unsafe static ReadContext LoadFromPly( string plyFile, bool showWarnings, bool doUnsafe )
        {
            ReadContext rc = new ReadContext();
            rc.data = File.ReadAllBytes( plyFile );
            ReadHeader( ref rc );

            // Header validation
            {
                if ( !rc.HasPositions() )
                    throw new InvalidOperationException( "Could not find position index" );
                if ( !rc.HasNormals() && showWarnings )
                    Debug.LogWarning( "Model: " + plyFile + " does not contain normals" );
                if ( !rc.HasColors() && showWarnings )
                    Debug.LogWarning( "Model: " + plyFile + " does not contain colors" );
            }

            if ( rc.isBinary )
            {
                ReadBinary( ref rc );
            }
            else
            {
                ReadAscii( ref rc, doUnsafe );
            }

            // Validation checks
            if ( rc.indices != null && rc.numFaces * 3 != rc.indices.Count && rc.numFaces * 4 != rc.indices.Count )
                throw new InvalidDataException( "invalid indices count" );

            if ( rc.numVertices * rc.numVertexAttributes != rc.vertexData.Count )
                throw new InvalidDataException( "invalid vertex count" );

            return rc;
        }

        static void ReadHeader( ref ReadContext rc )
        {
            rc.normalOffset   = new Vector3Int( -1, -1, -1 );
            rc.positionOffset = new Vector3Int( -1, -1, -1 );
            rc.colorOffset    = new Vector3Int( -1, -1, -1 );
            rc.alphaOffset    = -1;
            rc.isBinary       = false;
            rc.numFaces       = -1;
            rc.numVertexAttributes      = 0;
            rc.vertexDataTypes          = new List<ReadType>();
            var typeNames     = Enum.GetNames( typeof( ReadType ) ).Select(x => x.ToLowerInvariant()).ToList();
            var typeTypes     = Enum.GetValues( typeof( ReadType ) ).Cast<ReadType>().ToList();
            var oldPosition   = rc.readPosition;
            while ( true )
            {
                if ( rc.data[rc.readPosition] == '\n' )
                {
                    string line = Encoding.ASCII.GetString( rc.data, oldPosition, rc.readPosition-oldPosition );
                    line = line.ToLowerInvariant();
                    oldPosition = rc.readPosition+1;
                    var args = line.Split( new char[] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries );
                    if ( args[0] == "format" )
                    {
                        if ( line.Contains( "binary" ) )
                        {
                            rc.isBinary = true;
                        }
                        else if ( line.Contains( "ascii" ) )
                        {
                            rc.isBinary = false;
                        }
                        else
                        {
                            throw new InvalidOperationException( "invalid format" );
                        }
                    }
                    else if ( args[0] == "property" )
                    {
                        if ( rc.numFaces == -1 ) // If this is read, then property is no longer part of an vertex attribute
                        {
                            rc.vertexDataTypes.Add( typeTypes[typeNames.IndexOf( args[1] )] );
                            if ( args[2] == "x" || args[2] == "position_x" || args[2] == "vertex_x" ) rc.positionOffset.x = rc.numVertexAttributes;
                            else if ( args[2] == "y" || args[2] == "position_y" || args[2] == "vertex_y" ) rc.positionOffset.y = rc.numVertexAttributes;
                            else if ( args[2] == "z" || args[2] == "position_z" || args[2] == "vertex_z" ) rc.positionOffset.z = rc.numVertexAttributes;
                            else if ( args[2] == "nx" || args[2] == "normal_x" ) rc.normalOffset.x = rc.numVertexAttributes;
                            else if ( args[2] == "ny" || args[2] == "normal_y" ) rc.normalOffset.y = rc.numVertexAttributes;
                            else if ( args[2] == "nz" || args[2] == "normal_z" ) rc.normalOffset.z = rc.numVertexAttributes;
                            else if ( args[2] == "red"   || args[2] == "r" ) rc.colorOffset.x = rc.numVertexAttributes;
                            else if ( args[2] == "green" || args[2] == "g" ) rc.colorOffset.y = rc.numVertexAttributes;
                            else if ( args[2] == "blue"  || args[2] == "b" ) rc.colorOffset.z = rc.numVertexAttributes;
                            else if ( args[2] == "alpha" || args[2] == "a" ) rc.alphaOffset   = rc.numVertexAttributes;
                            rc.numVertexAttributes++;
                        }
                        else
                        {
                            if ( args[1] == "list" )
                            {
                                rc.indexCountType = typeTypes[typeNames.IndexOf( args[2] )];
                                rc.indicesType    = typeTypes[typeNames.IndexOf( args[3] )];
                            }
                            else
                            {
                                throw new InvalidOperationException( "Unhandled data detected" );
                            }
                        }
                    }
                    else if ( args[0] == "element" )
                    {
                        if ( args[1] == "vertex" )
                        {
                            rc.numVertices = int.Parse( args[2] );
                        }
                        else if ( args[1] == "face" )
                        {
                            rc.numFaces = int.Parse( args[2] );
                        }
                    }
                    else if ( args[0] == "end_header" )
                    {
                        rc.readPosition++; // Go to next line
                        return;
                    }
                }
                rc.readPosition++;
            }
            throw new InvalidOperationException( "Invalid header" );
        }

        private static void ReadVerticesBinary( ref ReadContext rc )
        {
            rc.vertexData = new List<double>( rc.numVertices );
            for ( int nVertex = 0; nVertex < rc.numVertices; nVertex++ )
            {
                for ( int i = 0; i < rc.vertexDataTypes.Count; i++ )
                {
                    switch ( rc.vertexDataTypes[i] )
                    {
                        case ReadType.Char:
                            rc.vertexData.Add( BitConverter.ToChar( rc.data, rc.readPosition ) );
                            rc.readPosition += sizeof( char );
                            break;
                        case ReadType.Uchar:
                            rc.vertexData.Add( rc.data[rc.readPosition] );
                            rc.readPosition += sizeof( byte );
                            break;
                        case ReadType.Short:
                            rc.vertexData.Add( BitConverter.ToInt16( rc.data, rc.readPosition ) );
                            rc.readPosition += sizeof( short );
                            break;
                        case ReadType.UShort:
                            rc.vertexData.Add( BitConverter.ToUInt16( rc.data, rc.readPosition ) );
                            rc.readPosition += sizeof( ushort );
                            break;
                        case ReadType.Uint:
                            rc.vertexData.Add( BitConverter.ToUInt32( rc.data, rc.readPosition ) );
                            rc.readPosition += sizeof( uint );
                            break;
                        case ReadType.Float:
                            rc.vertexData.Add( BitConverter.ToSingle( rc.data, rc.readPosition ) );
                            rc.readPosition += sizeof( float );
                            break;
                        case ReadType.Double:
                            rc.vertexData.Add( BitConverter.ToDouble( rc.data, rc.readPosition ) );
                            rc.readPosition += sizeof( double );
                            break;

                        default:
                            throw new InvalidOperationException( "Invalid type" );
                    }
                }
            }
        }

        private static void ReadIndicesBinary( ref ReadContext rc )
        {
            if ( rc.numFaces < 0 )
                return;
            int numIndicesPerFace = 0;
            rc.indices = new List<int>( rc.numFaces );
            for ( int nFace = 0; nFace < rc.numFaces; nFace++ )
            {
                switch ( rc.indexCountType )
                {
                    case ReadType.Char:
                        numIndicesPerFace = BitConverter.ToChar( rc.data, rc.readPosition );
                        rc.readPosition += sizeof( char );
                        break;
                    case ReadType.Uchar:
                        numIndicesPerFace = rc.data[rc.readPosition];
                        rc.readPosition += sizeof( byte );
                        break;
                    default:
                        throw new InvalidOperationException( "Invalid type" );
                }

                for ( int j = 0; j < numIndicesPerFace; j++ )
                {
                    int index;
                    switch ( rc.indexCountType )
                    {
                        case ReadType.Short:
                            index = BitConverter.ToInt16( rc.data, rc.readPosition );
                            rc.readPosition += sizeof( short );
                            break;
                        case ReadType.UShort:
                            index = BitConverter.ToUInt16( rc.data, rc.readPosition );
                            rc.readPosition += sizeof( ushort );
                            break;
                        case ReadType.Int:
                            index = BitConverter.ToInt32( rc.data, rc.readPosition );
                            rc.readPosition += sizeof( int );
                            break;
                        case ReadType.Uint:
                            index = (int)BitConverter.ToUInt32( rc.data, rc.readPosition );
                            rc.readPosition += sizeof( uint );
                            break;
                        default:
                            throw new InvalidOperationException( "Invalid type" );
                    }
                    rc.indices.Add( index );
                }
            }
            if ( rc.indices.Count % numIndicesPerFace != 0 )
                throw new InvalidOperationException( "Unexpected index count detected" );
        }

        static void ReadBinary( ref ReadContext rc )
        {
            if ( !rc.isBinary )
                throw new InvalidOperationException( "Context is not binary" );

            ReadVerticesBinary( ref rc );
            ReadIndicesBinary( ref rc );
        }

        private static unsafe void ZeroWordAscii( char* pWord )
        {
            while ( *pWord != '\0' )
                *pWord++ = '\0';
        }

        private static unsafe void ReadWordAscii( ref ReadContext rc, char* pWord )
        {
            int bIdx = 0;
            var c = rc.data[rc.readPosition];
            while ( (c != ' ') && (c != '\t') && (c != '\r') && (c != '\n') )
            {
                pWord[bIdx] = (char) c;
                rc.readPosition++; bIdx++;
                c = rc.data[rc.readPosition];
            }
            if ( pWord[bIdx] == '\r' ) rc.readPosition++;
            pWord[bIdx] = '\0';
        }

        private static unsafe void SkipLineAscii( ref ReadContext rc )
        {
            var c = rc.data[rc.readPosition];
            while ( (c != '\r') && (c != '\n') )
            {
                rc.readPosition++;
                c = rc.data[rc.readPosition];
            }
            if ( rc.data[rc.readPosition] == '\r' ) rc.readPosition++;
            rc.readPosition++;
        }

        private static unsafe void ReadVerticesAscii( ref ReadContext rc, StreamReader sr, bool doUnsafe )
        {
            rc.vertexData = new List<double>( rc.numVertices );

            if ( !doUnsafe )
            {
                var delims = new char [] { ' ', '\t' };
                for ( int nVertex = 0; nVertex < rc.numVertices; nVertex++ )
                {
                    string line = sr.ReadLine();
                    string [] words = line.Split(delims, StringSplitOptions.RemoveEmptyEntries);
                    for ( int i = 0; i < Mathf.Min( words.Length, rc.vertexDataTypes.Count ); i++ )
                    {
                        rc.vertexData.Add( MiscUtils.ParseDouble( words[i] ) );
                    }
                    for ( int i = 0; i < rc.vertexDataTypes.Count - Mathf.Min( words.Length, rc.vertexDataTypes.Count ); i++ )
                    {
                        rc.vertexData.Add( 0 );
                    }
                }
            }
            else
            {
                sbyte* data = stackalloc sbyte[1024];
                string word = new string(data, 0, 1024, Encoding.ASCII);
                fixed ( char* pWord = word )
                {
                    for ( int nVertex = 0; nVertex < rc.numVertices; nVertex++ )
                    {
                        int numAttribsFound = 0;
                        for ( int i = 0; i < rc.vertexDataTypes.Count; i++ )
                        {
                            ZeroWordAscii( pWord );
                            ReadWordAscii( ref rc, pWord );
                            rc.vertexData.Add( MiscUtils.ParseDouble( word ) );
                            numAttribsFound++;
                            if ( rc.readPosition == rc.data.Length || rc.data[rc.readPosition++] == '\n' )
                                break;
                        }

                        // Fix invalid/missing data (this does occur)
                        for ( int i = 0; i < rc.vertexDataTypes.Count - numAttribsFound; i++ )
                        {
                            rc.vertexData.Add( 0 );
                        }
                    }
                }
            }
        }

        private static unsafe void ReadIndicesAscii( ref ReadContext rc, StreamReader sr, bool doUnsafe )
        {
            if ( rc.numFaces < 0 )
                return;
            rc.indices = new List<int>( rc.numFaces );

            if ( !doUnsafe )
            {
                var delims = new char [] { ' ', '\t' };
                for ( int nFace = 0; nFace < rc.numFaces; nFace++ )
                {
                    string line = sr.ReadLine();
                    string [] words = line.Split(delims, StringSplitOptions.RemoveEmptyEntries);
                    if ( MiscUtils.ParseInt( words[0] ) != 3 )
                        throw new InvalidDataException( "Only triangles supported" );
                    rc.indices.Add( MiscUtils.ParseInt( words[1] ) );
                    rc.indices.Add( MiscUtils.ParseInt( words[2] ) );
                    rc.indices.Add( MiscUtils.ParseInt( words[3] ) );
                }
            }
            else
            {
                sbyte* data = stackalloc sbyte[1024];
                string word = new string(data, 0, 1024, Encoding.ASCII);
                int numIndicesPerFace = 0;
                fixed ( char* pWord = word )
                {
                    for ( int nFace = 0; nFace < rc.numFaces; nFace++ )
                    {
                        int wordIdx = 0;
                        while ( true )
                        {
                            ZeroWordAscii( pWord );
                            ReadWordAscii( ref rc, pWord );
                            if ( nFace == 0 && wordIdx == 0 )
                            {
                                numIndicesPerFace = MiscUtils.ParseInt( word );
                                if ( !(numIndicesPerFace == 3) )
                                {
                                    throw new InvalidDataException( "Num indices per face must be 3 but is: " + numIndicesPerFace );
                                }
                            }
                            else if ( wordIdx >= 1 && wordIdx < numIndicesPerFace+1 )
                            {
                                rc.indices.Add( MiscUtils.ParseInt( word ) );
                                if ( wordIdx == numIndicesPerFace )
                                {
                                    var t = rc.indices[rc.indices.Count-1];
                                    rc.indices[rc.indices.Count-1] = rc.indices[rc.indices.Count-3];
                                    rc.indices[rc.indices.Count-3] = t;
                                }
                            }

                            if ( rc.readPosition == rc.data.Length || rc.data[rc.readPosition++] == '\n' )
                                break;

                            wordIdx++;
                        }
                    }
                }
            }

            if ( rc.indices.Count % 3 != 0 )
                throw new InvalidDataException( "Invalid number indices" );

            // Need to update this as num faces may be halved.
            rc.numFaces = rc.indices.Count / 3;
        }

        static void ReadAscii( ref ReadContext rc, bool doUnsafe )
        {
            if ( rc.isBinary )
                throw new InvalidOperationException( "Context is binary" );

      //      float t = Time.realtimeSinceStartup;
            using ( var sr = new StreamReader( new MemoryStream( rc.data, rc.readPosition, rc.data.Length-rc.readPosition, false ) ) )
            {
                ReadVerticesAscii( ref rc, sr, doUnsafe );
                ReadIndicesAscii( ref rc, sr, doUnsafe );
            }
      //      float t2 = Time.realtimeSinceStartup;
      //      Debug.Log( "Load time " + (t2-t) );
        }
    }
}