using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Wander
{
    public class PLYToPCache : MonoBehaviour
    {
        public string plyFile = "";
        public bool asBinary = false;

        public void Generate()
        {
            PLYUtils.CreatePointCacheFromPly( plyFile, asBinary );
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( PLYToPCache ) )]
    [InitializeOnLoad]
    public class PLYToPCacheEditor : Editor
    {
        static PLYToPCacheEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            PLYToPCache settings = (PLYToPCache)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select ply file" ) )
                {
                    settings.plyFile = EditorUtility.OpenFilePanel( settings.plyFile, "", "ply" );
                }
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    if ( !string.IsNullOrWhiteSpace( settings.plyFile ) )
                    {
                        settings.Generate();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
    }
#endif

}