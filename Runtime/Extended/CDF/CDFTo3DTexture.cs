using UnityEngine;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wander
{
    public class CDFTo3DTexture : MonoBehaviour
    {
        public string inputFile    = "u.nc";
        public string outputFolder = "";
        public string outputName = "out";
        public int specificBlock = -1;
        public float ratio = 1.0f;

        public void Generate()
        {
            IEnumerable<Texture3D> textures = NetCDFUtils.Create3DTexture( inputFile, ratio, specificBlock );
#if UNITY_EDITOR
            int t = 0;
            foreach ( var tex in textures )
            {

                string fout = Path.Combine( outputFolder, outputName + "_" + (++t) + ".asset" );
                AssetDatabase.CreateAsset( tex, fout.ToAssetsPath() );
            }
#endif
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( CDFTo3DTexture ) )]
    [InitializeOnLoad]
    public class CDFTo3DTextureEditor : Editor
    {
        static CDFTo3DTextureEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            CDFTo3DTexture settings = (CDFTo3DTexture)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select Input file" ) )
                {
                    settings.inputFile = EditorUtility.OpenFilePanel( settings.inputFile, "", "nc" );
                }
                if ( GUILayout.Button( "Select Output folder" ) )
                {
                    settings.outputFolder = EditorUtility.OpenFolderPanel( settings.outputFolder, "", "nc" );
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    settings.Generate();
                }
            }
            GUILayout.EndHorizontal();
        }
    }
#endif

}