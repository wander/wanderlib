﻿using Microsoft.Research.Science.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Wander
{
    class NetCDFUtils
    {
        // Assumes there is N blocks of 3d double data in the package. */
        public static IEnumerable<Texture3D> Create3DTexture( string cdfFile, float ratio = 1.0f, int specificSlice = -1 )
        {
            using ( var ds = DataSet.Open( cdfFile ) )
            {
                var vars = ds.Variables.Single( ( vt ) => vt.Rank == 4);
                int [] shape  = vars.GetShape();
                int [] slice  = new int[shape.Length];
                int [] origin = new int[shape.Length];
                shape.CopyTo( slice, 0 );
                shape.CopyTo( origin, 0 );
                slice[0]  = 0;
                origin[0] = 0;
                for ( int i = 0; i < origin.Length; i++ ) {
                    origin[i] = 0;
                }
                slice[0] = 1;

                // Take dimensions from a single input file
                int width  = shape[3];
                int depth  = shape[2];
                int height = shape[1];
                int dstWidth  = (int)( width*ratio );
                int dstHeight = (int)( height*ratio );
                int dstDepth  = (int)( depth*ratio );
                int slices    = shape[0];
                float [] flatten = new float[dstWidth*dstHeight*dstDepth];
                int t = 0;
                if ( specificSlice != -1 )
                {
                    t = specificSlice;
                    slices = t+1;
                }
                for ( ; t < slices; t++ )
                {
                    origin[0] = t;
                    Texture3D tex  = new Texture3D(dstWidth, dstHeight, dstDepth, TextureFormat.RFloat, false );
                    var u =   vars.GetData( origin, slice ) as Single [,,,];
                    for ( int z = 0; z < dstDepth; z++ )
                    {
                        for ( int y = 0; y < dstHeight; y++ )
                        {
                            for ( int x = 0; x < dstWidth; x++ )
                            {
                                flatten[(z * dstHeight*dstWidth + y*dstWidth + x)] = (float)u[0, (int)(y/ratio), (int)(z/ratio), (int)(x/ratio)];
                            }
                        }
                    }
                    tex.SetPixelData( flatten, 0 );
                    tex.Apply( false, true );
                    yield return tex;
                }
            }
        }

        /* Assumes there is 2d double data in the file. 
         */
        public static float[] CreateHeightmap( string cdfFile, out int width, out int height, string dataKey = "dem" )
        {
            width  = 0;
            height = 0;
            float [] heights;

            using ( var ds = DataSet.Open( cdfFile, ResourceOpenMode.ReadOnly ) )
            {
                var elevation = ds.Variables[dataKey].GetData() as double[,];

                width  = elevation.GetLength( 1 );
                height = elevation.GetLength( 0 );

                // heights as float
                heights = new float[width * height];
                for ( int y = 0; y < height; y++ )
                {
                    for ( int x = 0; x < width; x++ )
                    {
                        heights[y*width+x] = (float)elevation[y, x];
                    }
                }
            }

            return heights;
        }

        /* Assumes data to contain double data of dataKey'xxx'. 
         * MinMaxRemap normalizes the data as the unity terrain wants a normalized height.
         */
        public static float[,] CreateTerrain( string cdfFile, int resolution, string dataKey = "dem", float minHeight = -500, float maxHeight = 500 )
        {
            float [,] heights;

            using ( var ds = DataSet.Open( cdfFile, ResourceOpenMode.ReadOnly ) )
            {
                var elevation = ds.Variables[dataKey].GetData() as double[,];

                int srcWidth  = elevation.GetLength( 1 );
                int srcHeight = elevation.GetLength( 0 );

                // heights as float
                heights = new float[resolution, resolution];
                for ( int y = 0; y<resolution; y++ )
                {
                    float fy = (y+.5f) / (float)resolution;
                    int srcY = (int)( fy * srcHeight );
                    for ( int x = 0; x<resolution; x++ )
                    {
                        float fx = (x+.5f) / (float)resolution;
                        int srcX = (int)( fx * srcWidth );
                        heights[y, x] = (((float)elevation[srcY, srcX] - minHeight) / (maxHeight-minHeight)) -.5f;
                    }
                }
            }

            return heights;
        }
    }

}