using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wander
{
    public class CDFToHeightmap : MonoBehaviour
    {
        public string heightmap      = "dem.nc";
        public string outputFolder;
        public string keyElevation   = "dem";

        public void Generate()
        {
            float [] heights = NetCDFUtils.CreateHeightmap( heightmap, out int width, out int height, keyElevation );
            CreateHeightmap( width, height, heights );
        }

        void CreateHeightmap( int width, int height, float[] heights )
        {
            Texture2D heightmap = new Texture2D(width, height, TextureFormat.RFloat, false, true);
            heightmap.SetPixelData( heights, 0 );
            heightmap.Apply();
            byte [] exrBytes = heightmap.EncodeToEXR( Texture2D.EXRFlags.OutputAsFloat );

#if UNITY_EDITOR
            //    var path = Application.dataPath + "/#WANDER/CDF/heightmap.exr";
                 File.WriteAllBytes( outputFolder + "/mask.exr", exrBytes );

         //   AssetDatabase.CreateAsset( heightmap, outputFolder.ToAssetsPath() + "/mask" );
#endif
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( CDFToHeightmap ) )]
    [InitializeOnLoad]
    public class CDFToHeightmapEditor : Editor
    {
        static CDFToHeightmapEditor()
        {

        }

        public override void OnInspectorGUI()
        {
            CDFToHeightmap settings = (CDFToHeightmap)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select heightmap" ) )
                {
                    string s = EditorUtility.OpenFilePanel( settings.heightmap, "", "nc" );
                    if ( !string.IsNullOrEmpty( s ) )
                    {
                        settings.heightmap = s;
                    }
                }
                if ( GUILayout.Button( "Output folder" ) )
                {
                    string s = EditorUtility.OpenFolderPanel( settings.outputFolder, "", "nc" );
                    if ( !string.IsNullOrEmpty( s ) )
                    {
                        settings.outputFolder = s;
                    }
                }
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    if ( !string.IsNullOrWhiteSpace( settings.heightmap ) )
                    {
                        settings.Generate();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
    }

#endif
}