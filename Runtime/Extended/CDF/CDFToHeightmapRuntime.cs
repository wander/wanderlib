using UnityEngine;
using UnityEngine.VFX;

namespace Wander
{
    [ExecuteInEditMode()]
    public class CDFToHeightmapRuntime : MonoBehaviour
    {
        public string keyValuation = "dem";
        public VisualEffect visualEffect;
        public string heightmapKey = "heightmap";
        public Texture2D generatedHeightmap;

        private void Start()
        {
            float [] heights = NetCDFUtils.CreateHeightmap( Application.streamingAssetsPath + "/dem.nc", out int width, out int height, keyValuation );
            CreateHeightmap( width, height, heights );
        }

        void CreateHeightmap( int width, int height, float[] heights )
        {
            generatedHeightmap = new Texture2D(width, height, TextureFormat.RFloat, false, true);
            generatedHeightmap.SetPixelData( heights, 0 );
            generatedHeightmap.Apply();
            generatedHeightmap.name = "Auto_Generated";
            visualEffect.SetTexture( heightmapKey, generatedHeightmap );
        }
    }
}